<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <div class="row">
                <h3 class="caption"><?php the_title(); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
                <?php

                $args = array(
                        'blog_id'      => $GLOBALS['blog_id'],
                );

                $users = get_users( $args );
                echo '<table class="table chat">';
                ?>
                <thead>
                <tr>
                    <th class="right">#</th>
                    <th class="right"><?php echo _e('Avatar', 'aletheme'); ?></th>
                    <th class="right" ><?php echo _e('Uploader', 'aletheme'); ?></th>
                    <th class="right" ><?php echo _e('Ratings', 'aletheme'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $index = 1;
                foreach ($users as $user) {
                    $imgurl = get_field('img_avatar','user_'.$user->ID);
                    if(empty($imgurl)) {
                        $imgurl = get_avatar_url($user->ID);
                    }
                    ?>
                    <tr>
                        <td class="right"><?php echo $index; ?></td>
                        <td class="right"><img src="<?php echo $imgurl;?>" class="artist_img"></a></td>
                        <td class="right"><a href="<?php echo home_url() .  '/userprofile/?u='. $user->ID ?>"><?php echo $user->display_name; ?></a></td>
                        <td class="right"><?php echo $user->upload_files; ?></td>
                    </tr>
                    <?php
                    $index++;
                }
                echo '</tbody></table>';
                ?>
            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php get_sidebar('main-sidebar'); ?>
        </div>
    </div>

<?php get_footer();