</div>
<!--Content end-->
<footer class="footer">
    <div class="container footer_bot">
        <div class="col-md-3">
            <?php echo ale_option('copyrights'); ?> <br>
            <a href="mailto:<?php echo get_option('admin_email'); ?>"><?php echo get_option('admin_email'); ?></a>
        </div>
        <div class="col-md-9 ft">
            <p class="footer-text">
                <?php echo ale_option('txt_info'); ?>
            </p>
        </div>
    </div>
</footer>

<!--audiopleer-->
<footer class="footer_play">
    <div class="col-md-12 audio-player">
        <div class="player-playlist">
            <ul class="playlist-ul">
                <li>Текст <span>X</span></li>
            </ul>
        </div>
        <audio id="audio" ontimeupdate="initProgressBar()"></audio>
        <div class="row btn-group">
            <div class="player-controls scrubber">
                <button id="play" class="btn btn-custom">
                    <span title="Play" class="glyphicon glyphicon-play aligned"></span>
                </button>
                <button id="stop" class="btn btn-custom"><span title="Stop"
                                                               class="glyphicon glyphicon-stop aligned"></span>
                </button>

                <button id="prev" class="btn btn-custom"><span title="-1 second"
                                                               class="glyphicon glyphicon-chevron-left aligned"></span>
                </button>
                <button id="next" class="btn btn-custom"><span title="+1 second"
                                                               class="glyphicon glyphicon-chevron-right aligned"></span>
                </button>

                <div class="audio-wrapper" id="player-container" href="javascript:;">
                    <audio id="player" ontimeupdate="initProgressBar()">
                        <source src="" type="audio/mp3">
                    </audio>
                </div>
                <span id="seekObjContainer">
    			  <progress id="seekObj" value="0" max="1"></progress>
    			</span>
                <br>
                <small style="float: left; position: relative;" class="start-time"></small>
                <small style="float: right; position: relative;" class="end-time"></small>

                <button id="volumeup" class="btn btn-custom">
                    <span title="Volume Up" class="glyphicon glyphicon-plus aligned"></span>
                </button>
                <button id="volumedown" class="btn btn-custom">
                    <span title="Volume Down" class="glyphicon glyphicon-minus aligned"></span>
                </button>
                <button id="userPlaylist" class="btn btn-custom">
                    <span title="Volume Down" class="glyphicon glyphicon-list"></span>
                </button>
            </div>
        </div>
    </div>
</footer>
<div class="popup" id="pop-playlist">
    <span class="close">X</span>
    <?php ale_render_playlist(); ?>
</div>
<!-- Scripts -->
<?php wp_footer(); ?>
</body>
</html>