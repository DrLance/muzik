<?php
/*
  * Template name: Playlist
  * */
get_header(); ?>
    <div class="container-fluid main">
        <!-- left panel -->
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>
        </div>
        <!-- end left panel -->
        <div class="col-md-8 content_middle">
            <div class="container-fluid">
                <div class="row container-fluid">
                    <div class="container-fluid card_list album_list">
                        <h3 class="caption"><?php the_title(); ?></h3>
                        <?php $user = wp_get_current_user();
                        if ($user->ID !== 0) {
                            ?>
                            <div class="container-fluid row">
                                <form id="form-add-playlist" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <label for="imgPlaylist"><?php _e('Image of Playlist', 'aletheme'); ?></label>
                                    <input type="file" class="form-control-file" id="imgPlaylist">
                                </div>
                                <div class="form-group">
                                    <label for="namePlaylist"><?php _e('Name of Playlist', 'aletheme'); ?></label>
                                    <input type="text" class="form-control" id="namePlaylist"
                                           placeholder="<?php _e('Name of Playlist', 'aletheme'); ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="descriptionPlaylist"><?php _e('Description Playlist', 'aletheme'); ?></label>
                                    <textarea class="form-control" id="descriptionPlaylist" rows="3"></textarea>
                                </div>
                                <button id="submitPlaylist" type="submit"
                                        class="btn button-a"><?php _e('Submit', 'aletheme'); ?></button>
                                </form>
                            </div>
                            <br/>
                            <?php
                        } else {
                            ale_part('notfound');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php get_sidebar('main-sidebar'); ?>
        </div>
        <!--end right panel -->
    </div>

<?php get_footer();