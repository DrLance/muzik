<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>

            <div class="container-fluid">
                <h3><?php _e('Popular Artits', 'aletheme'); ?></h3>

                <?php global $wp_query, $paged;

                $paged = (get_query_var('paged'));

                $wp_query = null;
                $order = '';
                $wp_query = new WP_Query(array(
                    'posts_per_page' => 100,
                    'post_type' => 'artist',
                    'post_status' => 'publish',
                    'order_by' => 'post_title',
                    'paged' => $paged,
                ));

                $posts = $wp_query->posts;

                ?>

                <!--Pagination-->
                <?php ale_page_links_custom($wp_query); ?>
                <!--Pagination end-->

                <table class="table chat">
                    <thead>
                    <tr>
                        <th class="right">#</th>
                        <th class="right"><?php _e('Photo', 'aletheme'); ?></th>
                        <th class="right"><?php _e('Name', 'aletheme'); ?></th>
                        <th class="right"><?php _e('Tracks', 'aletheme'); ?></th>
                        <th class="right"><?php _e('Ratings', 'aletheme'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $index = 1;
                    foreach ($posts as $post) {
                        setup_postdata($post);
                        ?>
                    <tr>
                        <td class="right"><?php echo $index; ?></td>
                        <td><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="artist_img"></td>
                        <td class="right"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
                        <td class="right"><?php
                            $childrens = get_children(array(
                                'post_parent' => $post->ID,
                                'post_type' => 'attachment',
                                'numberposts' => -1,
                                'post_mime_type' => 'audio,application/x-flac'
                            ));

                            echo count($childrens);
                            ?>
                        </td>
                        <td class="right" style="width: 180px"><?php the_ratings(); ?></td>
                    </tr>
                    <?php
                    $index++;
                    }
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                    </tbody>
                </table>

            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>

<?php get_footer();