<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <div class="row">
                <h3 class="caption"><?php the_title(); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
                <?php
                if (isset($_GET['showall'])):

                    $args = array('hide_empty' => 0);

                else:

                    $page = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $per_page = 100;
                    $offset = ($page - 1) * $per_page;
                    $args = array('number' => $per_page, 'offset' => $offset, 'hide_empty' => 0);

                endif;

                $taxonomy = 'genre';
                $tax_terms = get_terms($taxonomy, $args);
                echo '<table class="table chat">';
                ?>
                <thead>
                <tr>
                    <th class="right">#</th>
                    <th><?php echo _e('Name', 'aletheme'); ?></th>
                    <th><?php echo _e('Count Album', 'aletheme'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $index = 1;
                foreach ($tax_terms as $tax_term) {
                    // echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf(__("View all posts in %s"), $tax_term->name) . '" ' . '>' . $tax_term->name . '</a></li>';
                    ?>

                    <tr>
                        <td class="right"><?php echo $index; ?></td>
                        <td><a href="<?php echo get_term_link( (int) $tax_term->term_id, 'genre' ); ?>"><?php echo $tax_term->name; ?></a></td>
                        <td><?php echo $tax_term->count; ?></td>
                    </tr>

                    <?php
                    $index++;
                }

                echo '</tbody></table>';

                if (!isset($_GET['showall'])):

                    $total_terms = wp_count_terms('genre');
                    $pages = ceil($total_terms / $per_page);

                    if ($pages > 1):
                        echo '<ul class="pagination pagination-sm">';

                        for ($pagecount = 1; $pagecount <= $pages; $pagecount++):
                            echo '<li><a href="' . get_permalink() . 'page/' . $pagecount . '/">' . $pagecount . '</a></li>';
                        endfor;
                        echo '</ul>';
                    endif;
                else:
                    echo '<a href="' . get_permalink() . '">show paged</a>';
                endif;
                ?>

            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>

<?php get_footer();