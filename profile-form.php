<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="container-fluid profile_edit" id="theme-my-login<?php $template->the_instance(); ?>">
    <?php $template->the_action_template_message( 'profile' ); ?>
    <form id="your-profile" action="<?php $template->the_action_url( 'profile', 'login_post' ); ?>" method="post" enctype="multipart/form-data">
        <?php wp_nonce_field( 'update-user_' . $current_user->ID ); ?>
        <p>
            <input type="hidden" name="from" value="profile" />
            <input type="hidden" name="checkuser_id" value="<?php echo $current_user->ID; ?>" />
        </p>

        <h3><?php echo esc_attr( $profileuser->user_login ); ?></h3>
        <div class="row avatar_edit">
            <div class="vis">
                <label for="img"><?php _e('Current Avatar', 'aletheme'); ?>:</label>
                <img name="img" src="<?php echo get_field('img_avatar','user_'.$current_user->ID); ?>" height="320px" width="320px"/>
            </div>
            <div class="row">
                <label class="control-label" for="file_avatar"><?php _e('Upload Avatar', 'aletheme'); ?>:</label>


                <input id="file_avatar" name="file_avatar" type="file" class="filestyle" data-text="Find file" accept="image/">
            </div>
        </div>

        <?php
        $state = __('Premium', 'aletheme');
        /*if (!pmpro_hasMembershipLevel('1')) {
            $state = __('Simple', 'aletheme');
        }*/
        ?>

        <div class="container-fluid table">
            <table>
                <tr>
                    <td><?php _e('Current Status', 'aletheme'); ?>:</td>
                    <td><?php echo $state; ?> <a href="#"><?php _e('Get Premium Status', 'aletheme'); ?></a></td>
                </tr>
                <tr>
                    <td>E-Mail:</td>
                    <td><?php echo $profileuser->user_email;  ?></td>
                </tr>
                <tr>
                    <td>VK аккаунт:</td>
                    <td>не привязан</td>
                </tr>
                <tr>
                    <td>FB аккаунт:</td>
                    <td>не привязан</td>
                </tr>
                <tr>
                    <td><?php _e('Hide E-Mail', 'aletheme'); ?>:</td>
                    <td>
                        <div class="checkbox">
                            <label>
                                <input name="hide_email" type="checkbox" <?php echo get_user_meta( $profileuser->ID, 'hide_email', true ); ?> >
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?php _e('Cencored', 'aletheme'); ?>:</td>
                    <td>
                        <div class="checkbox">
                            <label>
                                <input name="cencored" type="checkbox" <?php echo get_user_meta( $profileuser->ID, 'cencored', true ); ?> >
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?php _e('Favourite Genre', 'aletheme'); ?>:</td>
                    <td>
                        <div class="row">
                            <div class="selection_genre">
                                <select id="select_genre" class="form-control" name="genre">
                                    <option value=""><?php echo _e('Select Genre') ?></option>
                                    <?php ale_get_genres('genre'); ?>
                                </select>
                                <script>
                                    jQuery('#select_genre option[value=<?php echo get_user_meta( $profileuser->ID, 'genre', true ); ?>]').attr('selected', 'selected');
                                </script>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Skype:</td>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" name="skype" placeholder="Skype" value="<?php echo get_user_meta( $profileuser->ID, 'skype', true ); ?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?php _e('From', 'aletheme'); ?>:</td>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" name="city" placeholder="Город" value="<?php echo get_user_meta( $profileuser->ID, 'city', true ); ?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?php _e('Sex', 'aletheme'); ?>:</td>
                    <td>
                        <label class="radio-inline">
                            <input type="radio" name="male" value="male" <?php echo get_user_meta( $profileuser->ID, 'male', true ); ?>><?php _e('Male', 'aletheme'); ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="female" value="female" <?php echo get_user_meta( $profileuser->ID, 'female', true ); ?>><?php _e('Female', 'aletheme'); ?>
                        </label>
                    </td>
                </tr>
            </table>
                <p class="tml-submit-wrap">
                    <input type="hidden" name="action" value="profile"/>
                    <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>"/>
                    <input type="hidden" name="user_id" id="user_id"
                           value="<?php echo esc_attr($current_user->ID); ?>"/>
                    <input type="submit" class="button-a form-control"
                           value="<?php esc_attr_e('Update Profile', 'theme-my-login'); ?>" name="submit" id="submit"/>
                </p>
        </div>
    </form>
</div>
