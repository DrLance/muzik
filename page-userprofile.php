<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">
            <?php
            $user = wp_get_current_user();

            if (isset($_GET['u'])) {
                $user = get_user_by('id', $_GET['u']);
                if(!$user) {
                    $user = wp_get_current_user();
                }
            }

            $femmale = '';

            $hide = get_user_meta( $user->ID, 'hide_email', true );
            $skype = get_user_meta( $user->ID, 'skype', true );
            $from = get_user_meta( $user->ID, 'city', true );
            $genre = '';
            $genre_id = get_user_meta( $user->ID, 'genre', true );
            $email = '';
            $state = __('Premium');

            /*if (!pmpro_hasMembershipLevel('1', $user->ID)) {
                $state = __('Simple');
            }*/

            if(!get_user_meta( $user->ID, 'hide_email', true )) {
                $email = $user->user_email;
            }

            if($genre_id) {
                $genre = get_term((int)$genre_id)->name;
            }

            if(get_user_meta( $user->ID, 'male', true )) {
                $femmale = 'Man';
            }

            if(get_user_meta( $user->ID, 'female', true )) {
                $femmale = 'Women';
            }
            $imgurl = get_field('img_avatar','user_'.$user->ID,false);


            if(empty($imgurl)) {
                $imgurl = get_avatar_url($user->ID);
            }

            ?>
            <div class="container-fluid profile_page">
                <div class="row">
                    <div class="container-fluid vis">
                        <img src="<?php echo $imgurl; ?>" height="120px" width="120px">
                    </div>
                    <div class="container-fluid profile_name">
                        <div class="row">
                            <h3><?php echo $user->display_name; ?></h3>
                            <a href="#" title="Написать сообщение">
                                <div class="glyphicon glyphicon-envelope"></div>
                            </a>
                        </div>
                        <div class="row status"><?php _e('Status', 'aletheme'); ?> : <?php echo $state; ?></div>
                        <?php //if (!pmpro_hasMembershipLevel('1', $user->ID)) { ?>
                            <div class="row">
                                <button type="button" name="button" class="button-a v2">
                                    <span><?php _e('Get Premium Status', 'aletheme'); ?></span>
                                </button>
                            </div>
                        <?php //} ?>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>ID:</td>
                            <td><?php _e('Registered', 'aletheme'); ?>:</td>
                            <td><?php _e('Favourite Genre', 'aletheme'); ?>:</td>
                            <td>Email:</td>
                            <td>Skype:</td>
                            <td><?php _e('From', 'aletheme'); ?>:</td>
                            <td><?php _e('Sex', 'aletheme'); ?>:</td>
                            <td><?php _e('Ratings', 'aletheme'); ?>:</td>
                        </tr>
                        <tr>
                            <td><?php echo $user->ID; ?></td>
                            <td><?php echo $user->user_registered; ?></td>
                            <td><?php echo $genre; ?></td>
                            <td><?php echo $email; ?></td>
                            <td><?php echo $skype; ?></td>
                            <td><?php echo $from; ?></td>
                            <td><?php echo $femmale; ?></td>
                            <td><?php echo $user->upload_files; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <a href="<?php echo home_url() . '/useruploads/?u=' . $user->ID; ?>"><?php _e('All Upload Albums', 'aletheme') ?></a><br>
                    <a href="<?php echo home_url() . '/userplaylists/?u=' . $user->ID; ?>"><?php _e('Playlist User', 'aletheme'); ?></a><br>
                    <a href="<?php echo home_url() . '/usercomments/?u=' . $user->ID; ?>"><?php _e('Comments User', 'aletheme'); ?></a><br>
                </div>
            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php get_sidebar('main-sidebar'); ?>
        </div>
    </div>

<?php get_footer();