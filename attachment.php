<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <h3><?php the_title(); ?></h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="album_img">
                            <a href="#">
                                <?php
                                $album_id = get_post_meta($post->ID, 'audio_album_id', false);
                                $artist_id = $post->post_parent;
                                $album = get_post($album_id[0]);
                                $artist  = get_post($artist_id);
                                $thumb = get_the_post_thumbnail($album_id[0], array(270, 250));

                                if ($thumb) {
                                    echo get_the_post_thumbnail($album_id[0], array(270, 248));
                                }
                                ?>
                            </a>
                        </div>
                        <div class="center-block">

                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-7">
                        <div class="container-fluid">
                            <p>
                                На этой странице Вы можете бесплатно скачать песню <?php  echo $post->post_title; ?> в формате mp3, а также слушать ее онлайн.
                            </p>
                            <p><span class="country"><?php _e('Genre:','aletheme') ?></span>
                                <?php

                                $tmp = wp_get_post_terms($album_id[0], 'genre');
                                $prx = '';
                                $lastElement = end($tmp);
                                $meta = wp_read_audio_metadata($post->ID);

                                print_r($meta);

                                if(count($tmp) > 1) {
                                    $prx = ' / ';
                                }
                                foreach ($tmp as $tm) {
                                    if($tm == $lastElement) {
                                        $prx = '';
                                    }
                                    echo $tm->name . $prx;
                                }

                                ?>
                            </p>
                            <p><span class="country"><?php _e('Artist:','aletheme') ?></span>
                                <?php
                                $user = get_user_by('id', $post->post_author);
                                $date = get_the_date( 'd.m.Y', $post->ID );
                                ?>
                                <a href="<?php echo get_page_link($artist); ?>"><?php echo $artist->post_title; ?></a>
                            </p>
                            <p><span class="country"><?php _e('Album:','aletheme') ?></span>
                                <a href="<?php echo get_page_link($album); ?>"><?php echo $album->post_title; ?></a>
                            </p>
                            <p><span class="country"><?php _e('Duration:','aletheme') ?></span>
                                <?php echo $meta['length_formatted']; ?>
                            </p>
                            <p><span class="country"><?php _e('Size:','aletheme') ?></span>
                                <?php echo size_format($meta['filesize'], 2);?>
                            </p>
                            <p><span class="country"><?php _e('Uploader:','aletheme') ?></span>
                                <a href="<?php echo home_url() .  '/userprofile/?u='. $user->ID; ?>"><?php echo $user->display_name; ?></a>
                            </p>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row player-inline">
                    <?php
                    $len = $meta['length_formatted'];
                    $filesize = size_format($meta['filesize'], 2);
                    $bit = size_format($meta['bitrate']);

                    ?>

                    <div class="col-md-1">
                        <button data-audio="<?php echo $post->guid; ?>"
                                class="btn btn_play_stop">
                                    <span title="Play" class="glyphicon glyphicon-play aligned">
                                    </span>
                        </button>
                    </div>
                    <div class="col-md-9 details">
                        <div class="row">
                            <div class="artist">
                                <a href="<?php echo get_page_link($album);?>" class="container-fluid song_name">
                                    <?php echo $album->post_title; ?>
                                </a>
                            </div>
                            <span class="audio_size"><?php echo $filesize; ?></span>
                        </div>

                        <div class="row">
                            <div class="album_name">
                                <span class="glyphicon glyphicon-cd"></span>
                                <a href="<?php echo get_page_link($artist); ?>">
                                    <?php echo $artist->post_title; ?>
                                </a>
                            </div>
                            <div class="icons">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 control">
                        <div class="row">
                            <a href="<?php echo $post->guid ; ?>"><?php _e('Download','aletheme') ?></a>
                            <span class="save_to_pl glyphicon glyphicon-chevron-down"></span>
                            <span class="add_to_pl glyphicon glyphicon-plus-sign"></span>
                        </div>
                        <div class="row time_size">
                            <div class="time_song"><?php echo $len; ?></div>
                            <div class="size_file"><?php echo $bit; ?></div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <?php echo do_shortcode('[TheChamp-Sharing]') ?>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <h3>
                            <?php _e('VIDEO','aletheme'); ?>
                        </h3>

                        <?php
                        $you = get_post_meta($post->ID, 'you_tube', false);

                        if (count($you) > 0):
                            ?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="<?php echo $you; ?>"></iframe>
                            </div>
                        <?php else: ?>
                        <?php endif; ?>

                    </div>
                    <?php comments_template(); ?>
                    <div class="container-fluid card_list album_list">
                        <?php get_similar_albums($post->ID); ?>
                    </div>
                    <div class="container-fluid lastsongs">
                        <h3><?php _e('Last Downloaded','aletheme'); ?></h3>
                        <?php get_last_downloaded(); ?>
                    </div>
                </div>

                <?php wp_reset_postdata();
                wp_reset_query(); ?>

            <?php endwhile; else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>

        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>