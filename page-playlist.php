<?php
/*
  * Template name: Playlist
  * */
get_header(); ?>
    <div class="container-fluid main">
        <!-- left panel -->
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>
        </div>
        <!-- end left panel -->
        <div class="col-md-8 content_middle">
            <div class="container-fluid">
                <div class="row container-fluid">
                    <?php $user=wp_get_current_user();
                    if($user->ID !== 0) {
                    ?>
                    <div class="container-fluid card_list album_list">
                        <h3 class="caption"><?php the_title(); ?></h3>
                        <div class="container-fluid row">
                            <div class="col-md-2">
                                <button class="btn button-a v4"><?php _e('Add Playlist', 'aletheme'); ?></button>
                            </div>
                        </div>
                        <br/>
                        <?php get_user_playlists();
                        } else {
                        ale_part('notfound');
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php get_sidebar('main-sidebar'); ?>
        </div>
        <!--end right panel -->
    </div>

<?php get_footer();