<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="tml tml-login" id="theme-my-login<?php $template->the_instance(); ?>">
    <?php $template->the_errors(); ?>
    <div class="container register_button">
        <!-- Trigger the modal with a button -->
        <div class="row">
            <button type="button" data-toggle="modal" data-target="#enter" class="button-a v3">
                <span><?php _e('Login', 'theme-my-login'); ?></span>
            </button>
            <!-- Modal -->
            <div class="modal fade" id="enter" style="display: none;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <form name="loginform" id="loginform<?php $template->the_instance(); ?>"
                                  action="<?php $template->the_action_url('login', 'login_post'); ?>" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="log"
                                           id="user_login<?php $template->the_instance(); ?>"
                                           value="<?php $template->the_posted_value('log'); ?>" size="20"
                                           placeholder="<?php
                                           if ('username' == $theme_my_login->get_option('login_type')) {
                                               _e('Username', 'theme-my-login');
                                           } elseif ('email' == $theme_my_login->get_option('login_type')) {
                                               _e('E-mail', 'theme-my-login');
                                           } else {
                                               _e('Username or E-mail', 'theme-my-login');
                                           }
                                           ?>"
                                    >
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pwd" value="" size="20" autocomplete="off"
                                           class="form-control" id="exampleInputPassword1"
                                           placeholder="<?php _e('Password', 'theme-my-login'); ?>">
                                </div>
                                <?php do_action('login_form'); ?>
                                <div class="checkbox">
                                    <label>
                                        <input name="rememberme" type="checkbox"
                                               id="rememberme<?php $template->the_instance(); ?>" value="forever">Запомнить
                                    </label>
                                </div>
                                <button type="submit" class="button-a v3 btn-default" name="wp-submit"
                                        id="wp-submit<?php $template->the_instance(); ?>"><?php _e('Login', 'theme-my-login'); ?>
                                </button>
                                <input type="hidden" name="redirect_to"
                                       value="<?php $template->the_redirect_url('login'); ?>"/>
                                <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>"/>
                                <input type="hidden" name="action" value="login"/>
                            </form>
                            <div class="link">
                                <a href="#"><?php esc_attr_e( 'Lost password', 'theme-my-login' ); ?></a><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal end-->
        </div>
    </div>
</div>

<div class="tml tml-register" id="theme-my-login<?php $template->the_instance(); ?>">
    <div class="row"><button type="button" data-toggle="modal" data-target="#registration"  class="button-a"><span><?php esc_attr_e( 'Register', 'theme-my-login' ); ?></span></button>
        <!-- Modal -->
        <div class="modal fade" id="registration"style="display: none;">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register', 'login_post' ); ?>" method="post">
                            <div class="form-group">
                                <label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username', 'theme-my-login' ); ?></label>
                                <input required type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" class="form-control" placeholder="<?php esc_attr_e( 'Username', 'theme-my-login' ); ?>">
                            </div>
                            <div class="form-group">
                                <label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'E-mail', 'theme-my-login' ); ?></label>
                                <input required type="email" class="form-control" placeholder="<?php esc_attr_e( 'Email', 'theme-my-login' ); ?>" name="user_email" id="user_email<?php $template->the_instance(); ?>" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20">
                            </div>

                            <div class="form-group">
                                <label for="pass1<?php $template->the_instance(); ?>"><?php _e('Password', 'aletheme'); ?></label>
                                <input required autocomplete="off" name="pass1" id="pass1<?php $template->the_instance(); ?>" class="form-control" size="20" value="<?php _e( 'Password', 'theme-my-login' ); ?>" type="password" />
                            </div>
                            <div class="form-group">
                                <label for="pass2<?php $template->the_instance(); ?>"><?php _e('Repeat Password', 'aletheme'); ?></label>
                                <input required autocomplete="off" name="pass2" id="pass2<?php $template->the_instance(); ?>" class="form-control" size="20" value="<?php _e( 'Confirm Password', 'theme-my-login' ); ?>" type="password" />
                            </div>

                            <button type="submit" class="button-a btn-default" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>"
                                    value="<?php esc_attr_e( 'Register', 'theme-my-login' ); ?>">
                                <span><?php esc_attr_e( 'Register', 'theme-my-login' ); ?></span>
                            </button>
                            <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
                            <input type="hidden" name="action" value="register" />
                        </form>
                        <div class="link">
                            <a href="<?php echo home_url() . '/'; ?>lostpassword"><?php esc_attr_e( 'Lost password', 'theme-my-login' ); ?></a><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal end-->
    </div>
    <br>
    <?php     wp_reset_postdata();    ?>

</div>