<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <div class="row">
                <?php
                $current_taxonomy = get_query_var( 'taxonomy' );
                $current_term = get_query_var( 'term' );
                $description = term_description();
                ?>
                <h3 class="caption"><?php echo $current_term; ?></h3>
                <h4><?php echo $description ?> </h4>

                <!--filter start-->
                <div class="filter">
                    <button class="button-a btn-filter" data-target="name" name="name" value="name_name"><span><?php _e('Name', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="ratings" value="ratings"><span><?php _e('Ratings', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="new" value="new"><span><?php _e('Newest', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="year" value="year"><span><?php _e('Year', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="count" value="count"><span><?php _e('Count', 'aletheme'); ?></span></button>
                </div>
                <!--filter end-->
                <script>
                    jQuery(document).ready(function(e){

                        jQuery(".button-a").click(function(e){
                            e.preventDefault();
                            var data = {
                                action: 'filter_genre',
                                current_taxonomy: '<?php echo $current_taxonomy; ?>',
                                current_term: '<?php echo $current_term; ?>',
                                filter_value: this.value
                            };
                            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php

                            jQuery.post(myajax.url, data, function(response) {
                                console.log(response);
                                location.reload();
                            });
                        });
                    });
                </script>

                <?php
                global $wp_query, $paged;

                $paged = ( get_query_var( 'paged' ) );

                $posts = $wp_query->posts;


                $artist = '';
                $childrens = '';
                $year_release = '';

                $artists = get_posts(array(
                    'post_type' => 'artist',
                    'numberposts' => -1,
                ));

                ale_page_links_custom($wp_query);


                ?>

                <div id="container-genre" class="container-fluid">
                    <table class="table chat table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="right"></th>
                            <th class="right"><?php _e('Artist'); ?></th>
                            <th class="right"><?php _e('Album', 'aletheme'); ?></th>
                            <th class="right"><?php _e('Year Release', 'aletheme'); ?></th>
                            <th class="right"><?php _e('Tracks', 'aletheme'); ?></th>
                            <th class="right"><?php _e('Ratings', 'aletheme'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($posts as $post) {
                            setup_postdata($post);
                            $year = wp_get_post_terms($post->ID, 'year_genre');

                            $year_release = '';
                            if($year)
                                $year_release = $year[0]->name;

                            foreach ($artists as $art) {
                                $post_metas = get_post_meta($art->ID, 'albums_id',false);
                                foreach ($post_metas as $item) {
                                    if ($item == $post->ID) {
                                        $artist = $art;
                                        $childrens = get_children(array(
                                            'post_parent' => $artist->ID,
                                            'post_type' => 'attachment',
                                            'post_mime_type' => 'audio,application/x-flac',
                                            'numberposts' => -1,
                                        ));
                                    }
                                }
                            }
                            ?>
                            <tr>
                                <td class="right"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="artist_img"></td>
                                <td class="right"><a href="<?php echo get_permalink($artist); ?>"><?php echo $artist->post_title; ?></a></td>
                                <td width="40%" class="right"><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title; ?></a></td>
                                <td class="right"><?php echo $year_release;  ?></td>
                                <td class="right"><?php echo count($childrens); ?></td>
                                <td><?php the_ratings(); ?></td>
                            </tr>
                            <?php
                        }
                        wp_reset_postdata();
                        wp_reset_query();
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>

<?php get_footer();