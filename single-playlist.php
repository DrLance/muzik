<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h3><?php the_title(); ?></h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="album_img">
                            <a href="#">
                                <?php
                                $user = wp_get_current_user();
                                $thumb = get_the_post_thumbnail($post->ID, array(270, 250));

                                if ($thumb) {
                                    echo get_the_post_thumbnail($post->ID, array(270, 250));
                                }
                                ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="container-fluid">
                            <p><span class="country"><?php _e('Added:', 'aletheme') ?></span>
                                <a href="<?php echo home_url() . '/userprofile/?u=' . $user->ID; ?>"><?php echo $user->display_name; ?></a>
                            </p>
                            <div class="container-fluid description_artist">

                                <p><?php the_content(); ?></p>

                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($post->post_author == $user->ID) { ?>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-6">
                            <button id="deletePlaylist" data-playlist-id="<?php echo $post->ID; ?>"
                                    class="btn button-a v-4"><?php _e('Delete Playlist', 'aletheme'); ?></button>
                        </div>
                        <div class="col-md-3">
                            <button id="#editPlaylist" data-playlist-id="<?php echo $post->ID; ?>"
                                    class="btn button-a v-4"><?php _e('Edit Playlist', 'aletheme'); ?></button>
                        </div>
                    </div>
                <?php } ?>
                <hr class="hr_list">
                <div class="container-fluid">
                    <?php
                    $meta = get_post_meta($post->ID, 'playlist_track_id');
                    global $wp_query, $paged;

                    $paged = (get_query_var('paged'));

                    $wp_query = null;
                    $order = '';

                    $wp_query = new WP_Query(array(
                        'posts_per_page' => 8,
                        'post_type' => 'attachment',
                        'post__in' => empty($meta) ? [-1] : $meta,
                        'post_mime_type' => 'audio,application/x-flac',
                        'post_status' => 'any',
                        'order_by' => 'post_title',
                        'paged' => $paged,
                    ));

                    ale_page_links_custom($wp_query); ?>

                    <div class="container-fluid top_play">
                        <?php
                        if ($wp_query->have_posts()) :
                            while ($wp_query->have_posts()) : $wp_query->the_post();
                                setup_postdata($post);
                                $url = wp_get_attachment_url($post->ID);
                                $path = get_attached_file($post->ID);
                                //$metadata = wp_read_audio_metadata($path);
                                $metadata = array('length_formatted' => 0, 'filesize' => 0, 'bitrate' => 0);
                                $len = $metadata['length_formatted'];
                                $filesize = size_format($metadata['filesize'], 2);
                                $bit = size_format($metadata['bitrate']);
                                ?>
                                <div class="row player-inline">
                                    <?php
                                    $album_id = get_post_meta($post->ID, 'audio_album_id', true);
                                    $album_post = get_post($album_id);
                                    ?>

                                    <div class="col-md-1">
                                        <button data-audio="<?php echo $url; ?>"
                                                class="btn btn_play_stop">
                                    <span title="Play" class="glyphicon glyphicon-play aligned">
                                    </span>
                                        </button>
                                    </div>
                                    <div class="col-md-9 details">
                                        <div class="row">
                                            <div class="artist">
                                                <a href="<?php the_permalink(); ?>" class="container-fluid song_name">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                            <span class="audio_size"><?php echo $filesize; ?></span>
                                        </div>

                                        <div class="row">
                                            <div class="album_name">
                                                <span class="glyphicon glyphicon-cd"></span>
                                                <a href="<?php echo get_page_link($album_post) ?>">
                                                    <?php echo $album_post->post_title; ?>
                                                </a>
                                            </div>
                                            <div class="icons">
                                                <a href="#" class="ico_clip glyphicon glyphicon-facetime-video"
                                                   title=""></a>
                                                <a href="#" class="ico_text glyphicon glyphicon-align-left"
                                                   title=""></a>
                                            </div>
                                            <div class="icons">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 control">
                                        <div class="row">
                                            <a href="<?php the_permalink(); ?>"><?php _e('Download', 'aletheme') ?></a>
                                            <span class="save_to_pl glyphicon glyphicon-chevron-down"></span>
                                            <span class="add_to_pl glyphicon glyphicon-plus-sign"></span>
                                        </div>


                                        <div class="row time_size">
                                            <div class="time_song"><?php echo $len; ?></div>
                                            <div class="size_file"><?php echo $bit; ?> </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                            wp_reset_postdata();
                            wp_reset_query();
                        endif;
                        ?>
                    </div>
                    <div class="container-fluid lastsongs">
                        <h3><?php _e('Last Downloaded', 'aletheme') ?></h3>
                        <?php get_last_downloaded(); ?>
                    </div>

                </div>

            <?php endwhile; else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>

        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>