<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <h3><?php the_title(); ?></h3>

                <hr class="hr_list">
                <div class="container-fluid">
                    <?php the_content(); ?>
                </div>

            <?php endwhile; else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>