<?php
// include system functions
require_once ALETHEME_PATH . '/constants.php';

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

if (!isset( $content_width)) {
	$content_width = 1000; // default content width
}