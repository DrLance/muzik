<?php
/**
 * Most Commented Widget
 */
class Aletheme_Genre_Widget extends WP_Widget
{
    /**
     * General Setup
     */
    public function __construct() {

        /* Widget settings. */
        $widget_ops = array(
            'classname' => 'ale_genre_widget',
            'description' => 'A widget that displays your most commented posts'
        );

        /* Widget control settings. */
        $control_ops = array(
            'width'		=> 300,
            'height'	=> 350,
            'id_base'	=> 'ale_genre_widget'
        );

        /* Create the widget. */
        parent::__construct( 'ale_genre_widget', 'Жанры', $widget_ops, $control_ops );
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance )
    {
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );

        /* Our variables from the widget settings. */
        $number = $instance['number'];

        /* Before widget (defined by themes). */
        echo '<div class="container-fluid side_nav">';

        ?>
        <?php /* Display the widget title if one was input (before and after defined by themes). */
        if ( $title )
            echo $before_title . $title. $after_title;


        $args = array(
            'taxonomy'      => array( 'genre'), // название таксономии с WP 4.5
            'orderby'       => 'id',
            'order'         => 'ASC',
            'number'        => '12',
            'hide_empty' => true,
        );

        $terms = get_terms( $args );

        if( $terms && ! is_wp_error($terms) ){
            echo "<ul>";
            foreach( $terms as $term ){
                echo "<li>";
                echo '<a href="' . get_term_link( (int) $term->term_id, 'genre' ) . '" title="' . sprintf(__('%s', 'my_localization_domain'), $term->name) . '">' . $term->name . '</a>';
                echo "</li>";

            }
            echo "</ul>";
        }


        /* After widget (defined by themes). */


        echo '<div class="more"><a href="' . home_url() . '/genres/">'. __('All genres', 'aletheme') .'</a></div>';

        echo '</div>';

    }

    /**
     * Update Widget
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );

        return $instance;
    }

    /**
     * Widget Settings
     * @param array $instance
     */
    public function form( $instance )
    {
        //default widget settings.
        $defaults = array(
            'title' => __('Genre', 'aletheme'),
            'number' => 3
        );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Posts to show:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" />
        </p>
        <?php
    }
}