<?php
/**
 * Most Commented Widget
 */
class Aletheme_Top_Widget extends WP_Widget
{
    /**
     * General Setup
     */
    public function __construct() {

        /* Widget settings. */
        $widget_ops = array(
            'classname' => 'ale_top_widget',
            'description' => 'Топ аплоадеры'
        );

        /* Widget control settings. */
        $control_ops = array(
            'width'		=> 300,
            'height'	=> 350,
            'id_base'	=> 'ale_top_widget'
        );

        /* Create the widget. */
        parent::__construct( 'ale_top_widget', 'Топ аплодеры', $widget_ops, $control_ops );
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance )
    {
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );

        /* Our variables from the widget settings. */
        $number = $instance['number'];

        /* Before widget (defined by themes). */
        echo '<div class="container-fluid side_block side_list">';
        // Display Widget
        ?>
        <?php /* Display the widget title if one was input (before and after defined by themes). */
        if ( $title )
            echo $before_title . $title. $after_title;

        add_action('pre_user_query', 'temp_replace');
        function temp_replace($query){
            $query->query_from = str_replace("post_type = 'post'", "post_type = 'artist'", $query->query_from );
        };


        $args = array(
            'number'       => '10',
            'orderby' => 'post_count',
            'order'        => 'DESC'
        );

        $users = get_users( 'orderby=post_count&number=10&order=DESC' );

        //$users = get_users( $args );

        remove_action('pre_user_query', 'temp_replace');

        echo '<ul>';

        foreach ($users as $user) {
            $args = array(
                'author'        =>  $user->ID,
                'post_type' => 'attachment',
                'post_mime_type' => 'audio,application/x-flac',
                'orderby' => 'post_count',
                'order'        => 'DESC',
                'numberposts' => -1,
            );
            $current_user_posts = get_posts( $args );

            $url = get_edit_user_link( $user->ID );
            $imgurl = get_field('img_avatar','user_'.$user->ID);

            if(empty($imgurl)) {
                $imgurl = get_avatar_url($user->ID);
            }

            echo '<li>';
            echo '<a href="' . home_url() .  '/userprofile/?u='. $user->ID .'">';
            echo '<span class="vis">' . '<img src="' . $imgurl . '" height="32px" width="32px"/>' . '</span>' . $user->display_name . '</a>';
            echo '<span class="aright">' . count($current_user_posts) .'</span>';
            echo '</li>';

        }

        echo '</ul>';


        echo '<div class="more"><a href="' . home_url() . '/users/">'. __('Top 300') .'</a></div>';
        /* After widget (defined by themes). */
        echo '</div>';

    }

    /**
     * Update Widget
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );

        return $instance;
    }

    /**
     * Widget Settings
     * @param array $instance
     */
    public function form( $instance )
    {
        //default widget settings.
        $defaults = array(
            'title' => __('Top', 'aletheme'),
            'number' => 3
        );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Posts to show:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" />
        </p>
        <?php
    }
}