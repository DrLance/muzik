<?php
/**
 * Most Commented Widget
 */
class Aletheme_Artist_Widget extends WP_Widget
{
    /**
     * General Setup
     */
    public function __construct() {

        /* Widget settings. */
        $widget_ops = array(
            'classname' => 'ale_artist_widget',
            'description' => 'Исполнители'
        );

        /* Widget control settings. */
        $control_ops = array(
            'width'		=> 300,
            'height'	=> 350,
            'id_base'	=> 'ale_artist_widget'
        );

        /* Create the widget. */
        parent::__construct( 'ale_artist_widget', 'Исполнители', $widget_ops, $control_ops );
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance )
    {
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );

        /* Our variables from the widget settings. */
        $number = $instance['number'];

        /* Before widget (defined by themes). */
        echo '<div class="container-fluid side_block side_list">';
        // Display Widget
        ?>
        <?php /* Display the widget title if one was input (before and after defined by themes). */
        if ( $title )
            echo $before_title . $title. $after_title;


        $args = array(
            'post_type'         => 'artist',
            'numberposts'       => '10',
            'post_status' => 'publish',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => 'ratings_average',
                    'value' => 0,
                    'compare' => '>',
                    'type' => 'NUMERIC'
                ),
            )
        );

        $posts = get_posts( $args );

        echo '<ul>';

        foreach ($posts as $post) {
            $thumbnail = get_the_post_thumbnail( $post->ID,array(32, 32) );

            echo '<li>';
            echo '<a href="' . get_the_permalink($post->ID) . '">';
            echo '<span class="vis">'. $thumbnail .'</span>' . $post->post_title . '</a>';
            echo '<span class="aright"></span>';
            echo '</li>';

        }

        echo '</ul>';


        echo '<div class="more"><a href="' . home_url() . '/artist/">'. __('All Artists','aletheme') .'</a></div>';
        /* After widget (defined by themes). */
        echo '</div>';

    }

    /**
     * Update Widget
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );

        return $instance;
    }

    /**
     * Widget Settings
     * @param array $instance
     */
    public function form( $instance )
    {
        //default widget settings.
        $defaults = array(
            'title' => __('Жанры', 'aletheme'),
            'number' => 3
        );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Posts to show:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" />
        </p>
        <?php
    }
}