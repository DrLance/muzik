<?php
/**
 * Most Commented Widget
 */
class Aletheme_News_Widget extends WP_Widget
{
    /**
     * General Setup
     */
    public function __construct() {

        /* Widget settings. */
        $widget_ops = array(
            'classname' => 'ale_news_widget',
            'description' => 'Новости'
        );

        /* Widget control settings. */
        $control_ops = array(
            'width'		=> 300,
            'height'	=> 350,
            'id_base'	=> 'ale_news_widget'
        );

        /* Create the widget. */
        parent::__construct( 'ale_news_widget', 'Новости', $widget_ops, $control_ops );
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance )
    {
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );

        /* Our variables from the widget settings. */
        $number = $instance['number'];

        /* Before widget (defined by themes). */
        echo '<div class="container-fluid side_block new_list">';
        // Display Widget
        ?>
        <?php /* Display the widget title if one was input (before and after defined by themes). */
        if ( $title )
            echo $before_title . $title. $after_title;


        $args = array(
            'post_type'         => 'news',
            'numberposts'       => '5',
            'post_status' => 'publish',
        );

        $posts = get_posts( $args );

        foreach ($posts as $post) {

            echo '<div class="row">';
            echo '<p></p>';
            echo '<p><a href="' . get_the_permalink($post->ID) . '">'. $post->post_title .'</a></p>';
            echo '<p>'. $post->post_content .'</p>';
            echo '</div>';

        }

        echo '<div class="more"><a href="' . home_url() . '/news/">'. __('Archive News', 'aletheme') .'</a></div>';
        /* After widget (defined by themes). */
        echo '</div>';

    }

    /**
     * Update Widget
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );

        return $instance;
    }

    /**
     * Widget Settings
     * @param array $instance
     */
    public function form( $instance )
    {
        //default widget settings.
        $defaults = array(
            'title' => __('Жанры', 'aletheme'),
            'number' => 3
        );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Posts to show:', 'aletheme') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" />
        </p>
        <?php
    }
}