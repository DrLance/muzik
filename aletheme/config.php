<?php
/**
 * Get current theme options
 *
 * @return array
 */
function aletheme_get_options()
{
    $comments_style = array(
        'wp' => 'Alethemes Comments',
        'fb' => 'Facebook Comments',
        'dq' => 'DISQUS',
        'lf' => 'Livefyre',
        'off' => 'Disable All Comments',
    );

    $headerfont = array_merge(ale_get_safe_webfonts(), ale_get_google_webfonts());

    $background_defaults = array(
        'color' => '',
        'image' => '',
        'repeat' => 'repeat',
        'position' => 'top center',
        'attachment' => 'scroll'
    );


    $imagepath = ALETHEME_URL . '/assets/images/';

    $options = array();

    $options[] = array("name" => __('Theme', 'aletheme'),
        "type" => "heading");

    $options[] = array("name" => __('Site Logo', 'aletheme'),
        "desc" => "Upload or put the site logo link (Default logo size: 133-52px)",
        "id" => "ale_sitelogo",
        "std" => "",
        "type" => "upload");

    $options[] = array("name" => __('Site Footer Logo', 'aletheme'),
        "desc" => "Upload or put the site logo link (Default logo size: 133-52px)",
        "id" => "ale_sitelogofooter",
        "std" => "",
        "type" => "upload");

    $options[] = array('name' => __('Manage Background', 'aletheme'),
        'desc' => "Select the background color, or upload a custom background image. Default background is the #f5f5f5 color",
        'id' => 'ale_background',
        'std' => $background_defaults,
        'type' => 'background');

    $options[] = array("name" => __('Show Site Preloader', 'aletheme'),
        "desc" => "Description kakoito.",
        "id" => "ale_backcover",
        "std" => "1",
        "type" => "checkbox");

    $options[] = array("name" => __('Upload Favicon', 'aletheme'),
        "desc" => "Upload or put the link of your favicon icon",
        "id" => "ale_favicon",
        "std" => "",
        "type" => "upload");

    $options[] = array("name" => __('Comments Style', 'aletheme'),
        "desc" => "Choose your comments style. If you want to use DISQUS comments please install and activate this plugin from <a href=\"" . admin_url('plugin-install.php?tab=search&type=term&s=Disqus+Comment+System&plugin-search-input=Search+Plugins') . "\">Wordpress Repository</a>.  If you want to use Livefyre Realtime Comments comments please install and activate this plugin from <a href=\"" . admin_url('plugin-install.php?tab=search&type=term&s=Livefyre+Realtime+Comments&plugin-search-input=Search+Plugins') . "\">Wordpress Repository</a>.",
        "id" => "ale_comments_style",
        "std" => "wp",
        "type" => "select",
        "options" => $comments_style);

    $options[] = array("name" => __('AJAX Comments', 'aletheme'),
        "desc" => "Use AJAX on comments posting (works only with Alethemes Comments selected).",
        "id" => "ale_ajax_comments",
        "std" => "1",
        "type" => "checkbox");

    $options[] = array("name" => __('Social Sharing', 'aletheme'),
        "desc" => "Enable social sharing for posts.",
        "id" => "ale_social_sharing",
        "std" => "1",
        "type" => "checkbox");

    $options[] = array("name" => __('Copyrights', 'aletheme'),
        "desc" => "Your copyright message.",
        "id" => "ale_copyrights",
        "std" => "",
        "type" => "editor");

    $options[] = array("name" => __('Home Page Slider Slug', 'aletheme'),
        "desc" => "Insert the slider slug. Get the slug on Sliders Section",
        "id" => "ale_homeslugfull",
        "std" => "",
        "type" => "text");

    $options[] = array("name" => __('Blog Slider Slug', 'aletheme'),
        "desc" => "Insert the slider slug. Get the slug on Sliders Section",
        "id" => "ale_blogslugfull",
        "std" => "",
        "type" => "text");

    $options[] = array("name" => __('Typography'),
        "type" => "heading");

    $options[] = array("name" => __('Select The Body Front', 'aletheme'),
        "desc" => "The default Font is - Raleway",
        "id" => "ale_headerfont",
        "std" => "Raleway",
        "type" => "select",
        "options" => $headerfont);

    $options[] = array("name" => __('Select The Body Front (Extend)', 'aletheme'),
        "desc" => "The default Font (extended) is - 600",
        "id" => "ale_headerfontex",
        "std" => "600",
        "type" => "text",
    );

    $options[] = array("name" => __('Select The Headers Font', 'aletheme'),
        "desc" => "The default Font is - Libre Baskerville",
        "id" => "ale_mainfont",
        "std" => "Libre+Baskerville",
        "type" => "select",
        "options" => $headerfont);

    $options[] = array("name" => __('Select The Headers Font (Extend)', 'aletheme'),
        "desc" => "The default Font (extended) is - 400,400italic",
        "id" => "ale_mainfontex",
        "std" => "400,400italic",
        "type" => "text",
    );

    $options[] = array('name' => __('H1 Style', 'aletheme'),
        'desc' => "Change the h1 style",
        'id' => 'ale_h1sty',
        'std' => array('size' => '22px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('H2 Style', 'aletheme'),
        'desc' => "Change the h2 style",
        'id' => 'ale_h2sty',
        'std' => array('size' => '20px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('H3 Style', 'aletheme'),
        'desc' => "Change the h3 style",
        'id' => 'ale_h3sty',
        'std' => array('size' => '18px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('H4 Style', 'aletheme'),
        'desc' => "Change the h4 style",
        'id' => 'ale_h4sty',
        'std' => array('size' => '16px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('H5 Style', 'aletheme'),
        'desc' => "Change the h5 style",
        'id' => 'ale_h5sty',
        'std' => array('size' => '14px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('H6 Style', 'aletheme'),
        'desc' => "Change the h6 style",
        'id' => 'ale_h6sty',
        'std' => array('size' => '12px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');

    $options[] = array('name' => __('Body Style', 'aletheme'),
        'desc' => "Change the body font style",
        'id' => 'ale_bodystyle',
        'std' => array('size' => '11px', 'face' => 'Libre+Baskerville', 'style' => 'normal', 'color' => '#111111'),
        'type' => 'typography');


    $options[] = array(
        "name" => __('Advanced Settings'),
        "type" => "heading");

    $options[] = array(
        "name" => __('Enable Moderate Upload File', 'aletheme'),
        "desc" => "Enable Moderate Upload files (mp3,flac, wav)",
        "id" => "ale_moderate_enabled",
        "std" => "",
        "type" => "checkbox");


    $options[] = array("name" => "Google Analytics",
        "desc" => "Please insert your Google Analytics code here. Example: <strong>UA-22231623-1</strong>",
        "id" => "ale_ga",
        "std" => "",
        "type" => "text");

    $options[] = array(
        "name" => __('Search Music', 'aletheme'),
        "desc" => "Section search Music",
        "id" => "ale_txt_search",
        "std" => "",
        "type" => "text"
    );

    $options[] = array(
        "name" => __('Search Music Option', 'aletheme'),
        "desc" => "Section search Music",
        "id" => "ale_txt_search_opt",
        "std" => "",
        "type" => "textarea"
    );

    $options[] = array(
        "name" => __('Listen Music', 'aletheme'),
        "desc" => "Section Listen Music",
        "id" => "ale_txt_listen",
        "std" => "",
        "type" => "text"
    );

    $options[] = array(
        "name" => __('Listen Music Option', 'aletheme'),
        "desc" => "Section Listen Music",
        "id" => "ale_txt_listen_opt",
        "std" => "",
        "type" => "textarea"
    );

    $options[] = array(
        "name" => __('Download Music', 'aletheme'),
        "desc" => "Section Download Music",
        "id" => "ale_txt_download",
        "std" => "",
        "type" => "text"
    );

    $options[] = array(
        "name" => __('Download Music Optopn', 'aletheme'),
        "desc" => "Section Download Music",
        "id" => "ale_txt_download_opt",
        "std" => "",
        "type" => "textarea"
    );

    $options[] = array(
        "name" => __('Info Field Footer', 'aletheme'),
        "desc" => "Information for release music",
        "id" => "ale_txt_info",
        "std" => "",
        "type" => "textarea"
    );


    return $options;
}

/**
 * Add custom scripts to Options Page
 */
function aletheme_options_custom_scripts()
{
    ?>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#ale_commentongallery').click(function () {
                jQuery('#section-ale_gallerycomments_style').fadeToggle(400);
            });
            if (jQuery('#ale_commentongallery:checked').val() !== undefined) {
                jQuery('#section-ale_gallerycomments_style').show();
            }
        });
    </script>

    <?php
}

/**
 * Add Metaboxes
 * @param array $meta_boxes
 * @return array
 */
function aletheme_metaboxes($meta_boxes)
{

    $meta_boxes = array();

    $prefix = "ale_";


    $meta_boxes[] = array(
        'id' => 'home_page_metabox',
        'title' => __('Home Meta Options', 'aletheme'),
        'pages' => array('page',), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on' => array('key' => 'page', 'value' => array('page.php'),), // Specific post templates to display this metabox

        'fields' => array(
            array(
                'name' => 'First Description',
                'desc' => 'Insert the text',
                'id' => $prefix . 'descr1',
                'type' => 'text',
            ),
            array(
                'name' => 'Second Description',
                'desc' => 'Insert the text',
                'id' => $prefix . 'descr2',
                'type' => 'textarea',
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'artist_post_metabox',
        'title' => 'Artist Second Decr',
        'pages' => array('artist'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on' => array('key' => 'artist', 'value' => array('single.php'),), // Specific post templates to display this metabox

        'fields' => array(
            array(
                'name' => 'Second Description',
                'desc' => 'Insert the text',
                'id' => $prefix . 'second_descr',
                'type' => 'textarea',
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'albums_id',
        'title' => __('Albums', 'aletheme'),
        'pages' => array('artist'), // Post type
        'context' => 'side',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on' => array('key' => 'artist', 'value' => array('single.php'),), // Specific post templates to display this metabox

        'fields' => array(
            array(
                'name' => __('Albums', 'aletheme'),
                'desc' => 'albums_id',
                'id' => $prefix . 'albums_id',
                'type' => 'albums_select',
                'meta_f' => 'albums_id',
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'audio_album_id',
        'title' => 'audio_album_id',
        'pages' => array('attachment'), // Post type
        'screen' => 'post ',
        'context' => 'bottom',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on' => array('key' => 'attachment', 'value' => array('attachment.php'),), // Specific post templates to display this metabox

        'fields' => array(
            array(
                'name' => 'audio_album_id',
                'desc' => 'audio_album_id',
                'id' => $prefix . 'audio_album_id',
                'type' => 'albums_select',
                'meta_f' => 'audio_album_id',
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'playlist_track_id',
        'title' => __('Трэки плейлиста', 'aletheme'),
        'pages' => array('playlist'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on' => array('key' => 'playlist', 'value' => array('single.php'),), // Specific post templates to display this metabox
        'fields' => array(
            array(
                'id' => $prefix . 'playlist_track_id',
                'type' => 'select',
                'meta_f' => 'playlist_track_id',
            ),
        )
    );

    return $meta_boxes;
}

/**
 * Get image sizes for images
 *
 * @return array
 */
function aletheme_get_images_sizes()
{
    return array(

        'gallery' => array(
            array(
                'name' => 'gallery-thumba',
                'width' => 430,
                'height' => 267,
                'crop' => true,
            ),
            array(
                'name' => 'gallery-mini',
                'width' => 100,
                'height' => 67,
                'crop' => true,
            ),
            array(
                'name' => 'gallery-big',
                'width' => 680,
                'height' => 9999,
                'crop' => false,
            ),
        ),
        'post' => array(
            array(
                'name' => 'post-thumba',
                'width' => 475,
                'height' => 295,
                'crop' => true,
            ),
            array(
                'name' => 'post-minibox',
                'width' => 500,
                'height' => 200,
                'crop' => true,
            ),
        ),


    );
}

/**
 * Add post types that are used in the theme
 *
 * @return array
 */
function aletheme_get_post_types()
{
    return array(
        'artist' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 20,
                'menu_icon' => 'dashicons-format-audio',
                'has_archive' => true,
                'rewrite' => true,
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                ),
                'show_in_nav_menus' => true,
            ),
            'singular' => __('Artist', 'aletheme'),
            'multiple' => __('Artists', 'aletheme'),
        ),
        'news' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 20,
                'menu_icon' => 'dashicons-welcome-widgets-menus',
                'has_archive' => true,
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                ),
                'show_in_nav_menus' => true,
            ),
            'singular' => __('News', 'aletheme'),
            'multiple' => __('News', 'aletheme'),
        ),
        'playlist' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 21,
                'menu_icon' => 'dashicons-welcome-widgets-menus',
                'has_archive' => true,
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                ),
                'show_in_nav_menus' => true,
            ),
            'singular' => __('Playlist', 'aletheme'),
            'multiple' => __('Playlists', 'aletheme'),
        ),
        'send_photo' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 21,
                'menu_icon' => 'dashicons-images-alt',
                'has_archive' => true,
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                ),
                'show_in_nav_menus' => true,
            ),
            'singular' => __('Sent photos', 'aletheme'),
            'multiple' => __('Sent photos', 'aletheme'),
        ),
    );
}

/**
 * Add taxonomies that are used in theme
 *
 * @return array
 */
function aletheme_get_taxonomies()
{
    return array(
        'genre' => array(
            'for' => array('post'),
            'config' => array(
                'sort' => true,
                'args' => array('orderby' => 'term_order', 'exclude_from_search' => false),
                'hierarchical' => true,
            ),
            'singular' => __('Genre', 'aletheme'),
            'multiple' => __('Genres', 'aletheme'),
        ),
        'year_genre' => array(
            'for' => array('post'),
            'config' => array(
                'sort' => true,
                'args' => array('orderby' => 'term_order'),
                'hierarchical' => true,
            ),
            'singular' => __('Year', 'aletheme'),
            'multiple' => __('Years', 'aletheme'),
        ),
        'country_artist' => array(
            'for' => array('artist'),
            'config' => array(
                'sort' => true,
                'args' => array('orderby' => 'term_order'),
                'hierarchical' => true,
                'has_archive' => true,
            ),
            'singular' => __('Country', 'aletheme'),
            'multiple' => __('Countries', 'aletheme'),
        ),
        'playlist_genre' => array(
            'for' => array('playlist'),
            'config' => array(
                'sort' => true,
                'args' => array('orderby' => 'term_order'),
                'hierarchical' => true,
                'has_archive' => true,
            ),
            'singular' => __('Genre', 'aletheme'),
            'multiple' => __('Genres', 'aletheme'),
        ),
    );
}

/**
 * Add post formats that are used in theme
 *
 * @return array
 */
function aletheme_get_post_formats()
{
    return array();
}

/**
 * Get sidebars list
 *
 * @return array
 */
function aletheme_get_sidebars()
{
    $sidebars = array();
    return $sidebars;
}

/**
 * Predefine custom sliders
 * @return array
 */
function aletheme_get_sliders()
{
    return array(
        'sneak-peek' => array(
            'title' => 'Sneak Peek',
        ),
    );
}

/**
 * Post types where metaboxes should show
 *
 * @return array
 */
function aletheme_get_post_types_with_gallery()
{
    return array();
}

/**
 * Add custom fields for media attachments
 * @return array
 */
function aletheme_media_custom_fields()
{
    return array(    );
}