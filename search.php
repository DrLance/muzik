<?php
/*
  * Template name: Home
  * */
get_header(); ?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs();
                $artists = get_posts(array(
                    'post_type' => 'artist',
                    'numberpost' => '-1',
                ));
                ?>
            </ol>
            <div class="row">
                <table class="table chat table-striped table-condensed">
                    <thead>
                    <tr>
                        <th class="right"></th>
                        <th class="right"><?php _e('Artist', 'aletheme'); ?></th>
                        <th><?php _e('Album', 'aletheme'); ?></th>
                        <th><?php _e('Year Release', 'aletheme'); ?></th>
                        <th><?php _e('Tracks', 'aletheme'); ?></th>
                        <th><?php _e('Ratings', 'aletheme'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (have_posts()) : while (have_posts()) : the_post();
                        $year_release = wp_get_post_terms($post->ID, 'year_genre');

                        foreach ($artists as $art) {
                            $post_metas = get_post_meta($art->ID, 'albums_id', false);
                            foreach ($post_metas as $item) {
                                if ($item == $post->ID) {
                                    $artist = $art;
                                    $childrens = get_children(array(
                                        'post_parent' => $artist->ID,
                                        'post_type' => 'attachment',
                                        'post_mime_type' => 'audio',
                                        'numberposts' => -1,
                                    ));
                                }
                            }
                        }
                        ?>
                        <tr>
                            <td class="right"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"
                                                   class="artist_img"></td>
                            <td class="right"><a
                                        href="<?php echo get_permalink($artist); ?>"><?php echo $artist->post_title; ?></a>
                            </td>
                            <td><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title; ?></a></td>
                            <td><?php echo $year_release[0]->name; ?></td>
                            <td><?php echo count($childrens); ?></td>
                            <td><?php the_ratings(); ?></td>
                        </tr>
                    <?php endwhile; else: ?>
                        <?php ale_part('notfound') ?>
                    <?php endif;
                    wp_reset_postdata();
                    wp_reset_query(); ?>
                    </tbody>
                </table>
            </div>
            <div class="container-fluid lastsongs">
            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->

<?php get_footer();

