<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>
            <div class="row">
                <h3><?php the_title(); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
            </div>
            <div class="container-fluid lastsongs">
            </div>
            <!--last album-->
            <div class="container-fluid">
                <div class="container-fluid card_list album_list">
                    <?php get_albums_top250(); ?>
                </div>
                <hr>
            </div>
            <!--last album end-->

            <div class="container-fluid lastsongs">
                <h3><?php _e('Last Downloaded', 'aletheme') ?></h3>
                <?php get_last_downloaded(); ?>
            </div>


        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->

<?php get_footer();

