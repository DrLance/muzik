<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <div class="row">
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
            </div>

            <div class="row soc_seti">
                <div class="container-fluid">
                    <?php echo do_shortcode('[TheChamp-Sharing]') ?>
                </div>
            </div>

            <div class="container-fluid top_play">
                <h3><?php _e('TOP SONGS','aletheme'); ?></h3>
                <div id="main" class="row player-inline" data-audio-cnt="20">
                    <div id="loader" class="row">
                    <div id="loading"><ul class="bokeh">
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    </div>
                    </div>
                    <script>

                    </script>
                </div>
            </div>

            <div class="row more"><a href="top100/"><?php _e('All TOP-100 Tracks','aletheme'); ?></a></div>
            <hr>

            <!--Top play lists-->
            <div class="row top_list">
                <ul>
                    <li class="active"><a href="#topAlbums" role="tab" data-toggle="tab" class="no-ajaxy"><?php _e('TOP Albums','aletheme'); ?></a></li>
                    <li><a href="#topCollections" role="tab" data-toggle="tab" class="no-ajaxy"><?php _e('TOP Compilations','aletheme'); ?></a></li>
                    <li><a href="#topSoundtracks" role="tab" data-toggle="tab" class="no-ajaxy"><?php _e('TOP Soundtracks','aletheme'); ?></a></li>
                </ul>
            </div>

            <div class="container-fluid card_list">
                <div class="tab-content">
                    <div role="tabpanel" id="topAlbums" class="tab-pane active container-fluid top_play">
                        <?php get_albums_ost('album'); ?>
                    </div>
                    <div role="tabpanel" id="topCollections" class="tab-pane container-fluid top_play">
                        <?php get_albums_ost('compilations'); ?>
                    </div>
                    <div role="tabpanel" id="topSoundtracks" class="tab-pane container-fluid top_play">
                        <?php get_albums_ost('ost'); ?>
                    </div>
                </div>
            </div>
            <hr>
            <!--Top play lists end-->
            <!--last album-->
            <div class="container-fluid">
                <h3><?php _e('Last uploaded','aletheme') ?></h3>
                <div class="container-fluid card_list album_list">
                    <?php get_albums_last(); ?>
                </div>
                <div class="more"><a href="#"><?php _e('All New','aletheme'); ?></a></div>
                <hr>
            </div>
            <!--last album end-->
            <div class="container-fluid lastsongs">
                <h3><?php _e('Last Downloaded','aletheme'); ?></h3>
                <?php get_last_downloaded(); ?>
            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->
    <!--      Instruction        -->
    <div class="container footer_instruction">
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_search'); ?></h4>
            <p><?php echo ale_option('txt_search_opt'); ?></p>
        </div>
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_listen'); ?></h4>
            <p><?php echo ale_option('txt_listen_opt'); ?></p>
        </div>
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_download'); ?></h4>
            <p><?php echo ale_option('txt_download_opt'); ?></p>
        </div>
    </div>
    <!--      Instruction end       -->

<?php get_footer();

