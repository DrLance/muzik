<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <div class="row">
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
            </div>

            <div class="row soc_seti">
                <div class="container-fluid">
                    <?php echo do_shortcode('[TheChamp-Sharing]') ?>
                </div>
            </div>

            <?php print_r(get_post_meta( 512, '_wp_attachment_meta', true )); ?>

            <div class="container-fluid top_play">
                <h3><?php _e('TOP SONGS','aletheme'); ?></h3>
                <div id="loader" class="row">
                <div id="loading">
                    <ul class="bokeh">
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    </div>
                    </div>
                <div id="main" class="row player-inline" data-audio-cnt="100">
                </div>
            </div>
            <hr>
            <!--last album end-->
            <div class="container-fluid lastsongs">
                <h3><?php _e('Last Downloaded','aletheme'); ?></h3>
                <?php get_last_downloaded(); ?>
            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->
    <!--      Instruction        -->
    <div class="container footer_instruction">
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_search'); ?></h4>
            <p><?php echo ale_option('txt_search_opt'); ?></p>
        </div>
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_listen'); ?></h4>
            <p><?php echo ale_option('txt_listen_opt'); ?></p>
        </div>
        <div class="col-md-4 nice-text">
            <h4><?php echo ale_option('txt_download'); ?></h4>
            <p><?php echo ale_option('txt_download_opt'); ?></p>
        </div>
    </div>
    <!--      Instruction end       -->

<?php get_footer();

