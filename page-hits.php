<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <div class="row">
                <h3 class="caption"><?php the_title(); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
                <hr>

                <div class="row top_list">
                    <ul>
                        <li class="active"><a href="#top7" role="tab" data-toggle="tab" class="no-ajaxy">ТОП-100 НЕДЕЛИ</a></li>
                        <li><a href="#top30" role="tab" data-toggle="tab" class="no-ajaxy">ТОП-100 МЕСЯЦА</a></li>
                    </ul>
                </div>
                <?php

                ?>
                <div class="tab-content">
                    <div role="tabpanel" id="top7" class="tab-pane active container-fluid top_play">
                        <div id="loader" class="row">
                            <div id="loading"><ul class="bokeh">
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <script>
                            (function($) {

                                $.ajax({
                                    url: myajax.url,
                                    type: 'post',
                                    data: {
                                        'action': 'ale_get_audios',
                                        'count_audio': 100
                                    },
                                    beforeSend: function() {
                                        $('#loader').show();
                                    },
                                    complete: function(){
                                        $('#loader').hide();
                                    },
                                    success: function( html ) {
                                        $('#top7').append( html );

                                        $('span[class *="save_to_pl"]').click(function() {
                                            var offset = $(this).offset();
                                            offset.left = offset.left + 15;
                                            $('#pop-playlist').css('display','inline-block').offset(offset);

                                        });

                                        $('.popup').click(function() {
                                            $('.popup').hide();
                                            $(this).removeAttr('style');
                                        })
                                    }
                                })

                            })(jQuery);
                        </script>

                    </div>
                    <div role="tabpanel" id="top30" class="tab-pane container-fluid top_play">
                        <div id="loader" class="row">
                            <div id="loading"></div>
                        </div>
                        <script>
                            (function($) {

                                $.ajax({
                                    url: myajax.url,
                                    type: 'post',
                                    data: {
                                        'action': 'ale_get_audios',
                                        'count_audio': 100
                                    },
                                    beforeSend: function() {
                                        $('#loader').show();
                                    },
                                    complete: function(){
                                        $('#loader').hide();
                                    },
                                    success: function( html ) {
                                        $('#top30').append( html );
                                    }
                                })

                            })(jQuery);
                        </script>

                    </div>
                </div>

            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>

<?php get_footer();