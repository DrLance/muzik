<?php get_header();
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/media.php'); ?>

    <div class="container-fluid main" xmlns="http://www.w3.org/1999/html">
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <?php if (have_posts() && is_user_logged_in()) : while (have_posts()) :
                the_post(); ?>
                <div class="container-fluid">
                    <?php
                    global $current_user;
                    wp_get_current_user();

                    $status = 'publish';

                    if(ale_get_option('moderate_enabled')){
                        $status = 'pending';
                    }

                    $args = array(
                        'author' => $current_user->ID,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'posts_per_page' => 1,
                        'post_type' => 'post',
                        'post_status' => $status
                    );

                    // get his posts 'ASC'
                    $current_user_posts = get_posts($args);
                    $album_id = $current_user_posts[0]->ID;
                    $title_album = $current_user_posts[0]->post_title;

                    wp_reset_postdata();
                    ?>
                    <p><?php echo _e('Add track to album ', 'aletheme'); ?></p>
                    <h3><?php echo $title_album; ?></h3>
                    <div class="container-fluid new_album">
                        <form id="upload" method="post" enctype="multipart/form-data"
                              action="<?php echo home_url() . '/upload/' ?>">
                            <input type="hidden" value="<?php echo $album_id ?>" name="album_id">

                            <span></span>
                            <div class="row form-group upload_file">
                                <span class="button-a form-control">
                                    <span>

                                        <input multiple id="fileupload" name="files" type="file"><?php _e('Add Files', 'aletheme'); ?> <i class="glyphicon glyphicon-plus"></i></input>
                                    </span>
                                </span>
                            </div>

                            <br>
                            <br>
                            <!-- The container for the uploaded files -->
                            <form method="post" enctype="multipart/form-data">
                                <div id="files" class="form-group"></div>
                            </form>
                        </form>
                    </div>
                    <a id="send-moderate" href="<?php echo home_url() . '/wp-admin/profile.php' ?>"
                       class="btn btn-success fileinput-button" style="display: none;">
                        <?php echo _e('Send moderate', 'aletheme'); ?>
                    </a>
                </div>
                <script>
                    jQuery(function () {
                        // Initialize the jQuery File Upload plugin
                        jQuery('#upload').fileupload({

                            // This function is called when a file is added to the queue;
                            // either via the browse button, or via drag/drop:
                            add: function (e, data) {

                                data.context = jQuery('<div />').appendTo('#files');
                                data.context.append('<div />');
                                jQuery.each(data.files, function (index, file) {
                                    var node = jQuery('<div />');
                                    if (!index) {
                                        node
                                            .append('<span class="text-span" />')
                                            .append(' <div id="progress" class="progress">\n' +
                                                '<div class="progress-bar progress-bar-success"></div>\n' +
                                                '</div>')
                                    }
                                    node.appendTo(data.context);
                                });

                                // Automatically upload the file once it is added to the queue
                                var jqXHR = data.submit();
                            },

                            progress: function (e, data) {

                                // Calculate the completion percentage of the upload
                                var progress = parseInt(data.loaded / data.total * 100, 10);

                                jQuery('#progress .progress-bar').css(
                                    'width',
                                    progress + '%'
                                );

                            },

                            fail: function (e, data) {
                                jQuery.each(data.files, function (index) {
                                    var error = jQuery('<span class="text-danger"/>').text('File upload failed.');
                                    jQuery(data.context.children()[index])
                                        .append('<br>')
                                        .append(error);
                                });
                            },

                            always: function (e, data) {

                                var resp = JSON.parse(data.jqXHR.responseText);
                                var tmpAudio = jQuery('<audio controls class="form-group" />');
                                tmpAudio.attr('src', resp.url);

                                var tmp = data.context.find('.text-span')
                                    .append('<label for="text" class="col-md-2"><?php _e('Artist:', 'aletheme') ?> </label>')
                                    .append('<input type="text" class="name_album form-control form-group" name="artist" value="' + resp.artist + '">')
                                    .append('<br>')
                                    .append('<label for="text" class="col-md-2"><?php _e('Name track:', 'aletheme') ?> </label>')
                                    .append('<input type="text" class="name_album form-control form-group" name="title" value="' + resp.title + '"></div>')
                                    .append('<br>')
                                    .append(tmpAudio);

                                tmp.appendTo(tmp);

                                data.context.find('button')
                                    .text('Upload')
                                    .prop('disabled', !!data.files.error);

                            },

                            done: function (e, data) {

                                jQuery.each(data.result.files, function (index, file) {
                                    if (file.url) {
                                        var link = jQuery('<a>')
                                            .attr('target', '_blank')
                                            .prop('href', file.url);
                                        jQuery(data.context.children()[index])
                                            .wrap(link);
                                    } else if (file.error) {
                                        var error = jQuery('<span class="text-danger"/>').text(file.error);
                                        jQuery(data.context.children()[index])
                                            .append('<br>')
                                            .append(error);
                                    }
                                });

                                jQuery('#send-moderate')
                                    .css('text-decoration', 'none')
                                    .css('display', 'block');
                            }

                        });

                        // Prevent the default action when a file is dropped on the window
                        jQuery(document).on('drop dragover', function (e) {
                            e.preventDefault();
                        });

                        // Helper function that formats the file sizes
                        function formatFileSize(bytes) {
                            if (typeof bytes !== 'number') {
                                return '';
                            }

                            if (bytes >= 1000000000) {
                                return (bytes / 1000000000).toFixed(2) + ' GB';
                            }

                            if (bytes >= 1000000) {
                                return (bytes / 1000000).toFixed(2) + ' MB';
                            }

                            return (bytes / 1000).toFixed(2) + ' KB';
                        }

                    });
                </script>

                <?php

                ?>
            <?php endwhile;
            else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>


<?php get_footer();