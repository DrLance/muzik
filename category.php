<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>
            <div class="row">
                <h3><?php the_title(); ?></h3>
                <?php if (have_posts()) : while (have_posts()) : the_post();
                    ?>
                    <?php ale_part('postpreview' );?>
                <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                <?php endif; ?>
            </div>
            <div class="container-fluid lastsongs">
            </div>
            <!--last album-->
            <div class="container-fluid">
                <div class="container-fluid card_list album_list">
                    <?php get_albums_top250(); ?>
                </div>
                <hr>
            </div>
            <!--last album end-->
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->

<?php get_footer();

