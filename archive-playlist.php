<?php
/*
  * Template name: Home
  * */
get_header(); ?>


    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>

        <div class="col-md-8 content_middle">

            <div class="row">
                <?php
                $current_taxonomy = get_query_var( 'taxonomy' );
                $current_term = get_query_var( 'term' );
                $description = term_description();
                ?>
                <h3 class="caption"><?php echo $current_term; ?></h3>
                <h4><?php echo $description ?> </h4>
                <!--filter start-->
                <div class="filter">
                    <button class="button-a btn-filter" data-target="name" name="name" value="name_name"><span><?php _e('Name', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="ratings" value="ratings"><span><?php _e('Ratings', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="new" value="new"><span><?php _e('Newest', 'aletheme'); ?></span></button>
                    <button class="button-a btn-filter" name="count" value="count"><span><?php _e('Count', 'aletheme'); ?></span></button>
                </div>
                <!--filter end-->

                <?php
                global $wp_query, $paged;
                $paged = ( get_query_var( 'paged' ) );
                $posts = $wp_query->posts;

                $childrens = '';

                $playlists = get_posts(array(
                    'post_type' => 'playlist',
                    'numberposts' => -1,
                ));

                ale_page_links_custom($wp_query);
                ?>

                <div id="container-genre" class="container-fluid">
                    <table class="table chat table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="right"></th>
                            <th class="right"><?php _e('Name Playlist', 'aletheme'); ?></th>
                            <th class="right"><?php _e('Tracks', 'aletheme'); ?></th>
                            <th class="right"><?php _e('Ratings', 'aletheme'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($posts as $post) {
                            setup_postdata($post);

                            $childrens = get_posts(array(
                                    'post_parent' => $post->ID,
                                    'post_type' => 'attachment',
                                    'post_mime_type' => 'audio,application/x-flac',
                                    'numberposts' => -1,
                                ));
                            ?>
                            <tr>
                                <td class="right"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="artist_img"></td>
                                <td class="right"><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title; ?></a></td>
                                <td class="right"><?php echo count($childrens); ?></td>
                                <td width="21%"><?php the_ratings(); ?></td>
                            </tr>
                            <?php
                        }
                        wp_reset_postdata();
                        wp_reset_query();
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>

<?php get_footer();