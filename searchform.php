<form role="search" method="get" id="searchform" action="<?php echo home_url(); ?>" class="navbar-form navbar-left">
    <div class="search-box">
        <div class="form-group">
            <input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s"
                   placeholder="<?php _e('Поиск...', 'aletheme') ?>"/>
        </div>
        <button type="submit" id="searchsubmit" class="btn btn-default" value="<?php _e('Search', 'aletheme') ?>"><?php _e('Search', 'aletheme') ?></button>
    </div>
</form>