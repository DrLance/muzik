<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>
            <div class="row">
                <h3><?php _e('News','aletheme'); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
            </div>
            <div class="container-fluid row">
                <?php
                $args = array(
                    'post_type'         => 'news',
                    'post_status' => 'publish',
                );
                $categories = get_posts( $args );
                if( $categories ){
                    foreach( $categories as $cat ){ ?>
                        <strong><?php echo get_the_date('d-m-Y',$cat); ?></strong>
                        <a href="<?php echo $cat->guid; ?>"><?php echo $cat->post_title; ?></a>
                        <br>
                        <hr />
                    <?php }
                }
                ?>
            </div>


        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->

<?php get_footer();

