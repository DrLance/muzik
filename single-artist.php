<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <h3><?php the_title(); ?></h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="album_img">
                            <a href="#">
                                <?php
                                $thumb = get_the_post_thumbnail($post->ID, array(270, 250));

                                if ($thumb) {
                                    echo get_the_post_thumbnail($post->ID, array(270, 250));
                                }
                                ?>
                            </a>
                        </div>
                        <div class="container register_button">


                            <?php if ($thumb) {
                                echo '<div class="row"><a href="#" class="button-a v3"><span>' . __('Subscribe', 'aletheme') . '</span></a></div>';
                            } else {
                                echo '<div class="row"><a href="#" class="button-a" data-toggle="modal" data-target="#modalSendPhoto"><span>' . __('Send Photo', 'aletheme') . '</span></a></div>';
                                echo '<div class="row"><a href="#" class="button-a v3"><span>' . __('Subscribe', 'aletheme') . '</span></a></div>';
                            }
                            ?>
                            <div class="center-block"><?php the_ratings(); ?></div>
                        </div>
                        <!-- Modal -->
                        <div id="modalSendPhoto" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?php _e('Send Photo', 'aletheme'); ?></h4>
                                    </div>
                                    <form id="formSubmitPhoto" data-artist-id="<?php echo $post->ID; ?>">
                                        <div class="modal-body">
                                            <img id="imgSendPhoto" src="" height="480px" width="520px"/>
                                            <label class="btn btn-default btn-file">
                                                <?php _e('Browse', 'aletheme'); ?>
                                                <input id="inputSendPhoto" required
                                                                                          accept="image/"
                                                                                          type="file"
                                                                                          style="display: none;">
                                            </label>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-default"><?php _e('Submit', 'aletheme'); ?></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="container-fluid">
                            <p><span class="country"><?php _e('Country:', 'aletheme') ?></span>
                                <?php
                                $tmp = wp_get_post_terms($post->ID, 'country_artist');
                                if ($tmp) {
                                    echo $tmp[0]->name;
                                }
                                ?>
                            </p>
                            <div class="container-fluid description_artist">

                                <p><?php the_content(); ?></p>

                                <div class="collapse" id="demo">
                                    <p><?php ale_meta('second_descr'); ?></p>
                                </div>
                                <button type="button" class="button-a all_text" data-toggle="collapse"
                                        data-target="#demo"><span><?php _e('All Text', 'aletheme') ?></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row album_nav">
                    <!--filter start-->
                    <button class="button-a"><span><?php _e('Songs', 'aletheme');
                            $childrens_audio = get_children(array(
                                'post_parent' => $post->ID,
                                'post_type' => 'attachment',
                                'numberposts' => -1,
                                'post_mime_type' => 'audio,application/x-flac',
                            ));

                            echo ' (' . count($childrens_audio) . ')';
                            ?> </span></button>
                    <button class="button-a" onclick='location.href="aartist"'>
                        <span>
                            <?php _e('Album', 'aletheme');
                            echo ' (' . count(get_post_meta($post->ID, 'albums_id')) . ')'; ?>
                        </span>
                    </button>
                    <button class="button-a" onclick='location.href="photo"'>
                        <span>
                            <?php _e('Photo', 'aletheme');
                            $childrens = get_children(array(
                                'post_parent' => $post->ID,
                                'post_type' => 'attachment',
                                'numberposts' => -1,
                                'post_mime_type' => 'image'
                            ));

                            echo ' (' . count($childrens) . ')';
                            ?>
                        </span>
                    </button>
                    <!--filter end-->
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <h3>
                            <?php _e('All Songs ', 'aletheme');
                            echo $post->post_title; ?> -
                            <span><?php echo count($childrens_audio); ?>
                            </span>
                        </h3>
                    </div>
                    <form method="post">
                        <div class="row filter">
                            <button type="submit" name="name" class="button-a btn-filter" data-target="name">
                                <span><?php _e('Name', 'aletheme') ?></span></button>
                            <button type="submit" name="ratings" class="button-a btn-filter">
                                <span><?php _e('Ratings', 'aletheme') ?></span></button>
                            <button type="submit" name="new" class="button-a btn-filter">
                                <span><?php _e('New', 'aletheme') ?></span>
                            </button>
                            <button type="submit" name="year" class="button-a btn-filter">
                                <span><?php _e('Year', 'aletheme') ?></span></button>
                        </div>
                    </form>
                </div>
                <hr class="hr_list">
                <div class="container-fluid">
                    <?php
                    $childrens = get_children(array(
                        'post_parent' => $post->ID,
                        'post_type' => 'attachment',
                        'numberposts' => 1,
                        'post_mime_type' => 'audio,application/x-flac',
                    ));

                    global $wp_query, $paged;

                    $paged = (get_query_var('paged'));

                    $wp_query = null;
                    $order = '';

                    if (isset($_POST['name'])) {
                        $wp_query = new WP_Query(array(
                            'posts_per_page' => 30,
                            'post_type' => 'attachment',
                            'post_parent' => $post->ID,
                            'post_mime_type' => 'audio,application/x-flac',
                            'post_status' => 'any',
                            'order_by' => 'post_title',
                            'paged' => $paged,
                        ));
                    } else if (isset($_POST['ratings'])) {
                        $wp_query = new WP_Query(array(
                            'posts_per_page' => 30,
                            'post_type' => 'attachment',
                            'post_parent' => $post->ID,
                            'post_mime_type' => 'audio,application/x-flac',
                            'post_status' => 'any',
                            'paged' => $paged,
                            'meta_key' => 'ratings_average',
                            'order_by' => 'meta_value_num',
                            'order' => 'DESC'
                        ));
                    } else if (isset($_POST['new'])) {
                        $wp_query = new WP_Query(array(
                            'posts_per_page' => 30,
                            'post_type' => 'attachment',
                            'post_parent' => $post->ID,
                            'post_mime_type' => 'audio,application/x-flac',
                            'post_status' => 'any',
                            'order_by' => 'post_date',
                            'paged' => $paged,
                        ));
                    } else if (isset($_POST['year'])) {
                        $wp_query = new WP_Query(array(
                            'posts_per_page' => 30,
                            'post_type' => 'attachment',
                            'post_parent' => $post->ID,
                            'post_mime_type' => 'audio,application/x-flac',
                            'post_status' => 'any',
                            'order_by' => 'post_date',
                            'paged' => $paged,
                        ));
                    } else {
                        $wp_query = new WP_Query(array(
                            'posts_per_page' => 30,
                            'post_type' => 'attachment',
                            'post_parent' => $post->ID,
                            'post_mime_type' => 'audio,application/x-flac',
                            'post_status' => 'any',
                            'paged' => $paged,
                        ));
                    }

                    ale_page_links_custom($wp_query); ?>

                    <div class="container-fluid top_play">
                        <?php
                        if ($wp_query->have_posts()) :
                            while ($wp_query->have_posts()) : $wp_query->the_post();
                                setup_postdata($post);
                                $url = wp_get_attachment_url($post->ID);
                                $path = get_attached_file($post->ID);
                                $metadata = wp_read_audio_metadata($path);
                                $len = $metadata['length_formatted'];
                                $filesize = size_format($metadata['filesize'], 2);
                                $bit = size_format($metadata['bitrate']);
                                ?>
                                <div class="row player-inline">
                                    <?php
                                    $album_id = get_post_meta($post->ID, 'audio_album_id', true);
                                    $album_post = get_post($album_id);
                                    ?>

                                    <div class="col-md-1">
                                        <button data-audio="<?php echo $url; ?>"
                                                class="btn btn_play_stop">
                                    <span title="Play" class="glyphicon glyphicon-play aligned">
                                    </span>
                                        </button>
                                    </div>
                                    <div class="col-md-9 details">
                                        <div class="row">
                                            <div class="artist">
                                                <a href="<?php the_permalink(); ?>" class="container-fluid song_name">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                            <span class="audio_size"><?php echo $filesize; ?></span>
                                        </div>

                                        <div class="row">
                                            <div class="album_name">
                                                <span class="glyphicon glyphicon-cd"></span>
                                                <a href="<?php echo get_page_link($album_post) ?>">
                                                    <?php echo $album_post->post_title; ?>
                                                </a>
                                            </div>
                                            <div class="icons">
                                                <a href="#" class="ico_clip glyphicon glyphicon-facetime-video"
                                                   title=""></a>
                                                <a href="#" class="ico_text glyphicon glyphicon-align-left"
                                                   title=""></a>
                                            </div>
                                            <div class="icons">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 control">
                                        <div class="row">
                                            <a href="<?php the_permalink(); ?>"><?php _e('Download', 'aletheme') ?></a>
                                            <span class="save_to_pl glyphicon glyphicon-chevron-down"></span>
                                            <span class="add_to_pl glyphicon glyphicon-plus-sign"></span>
                                        </div>


                                        <div class="row time_size">
                                            <div class="time_song"><?php echo $len; ?></div>
                                            <div class="size_file"><?php echo $bit; ?> </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                            wp_reset_postdata();
                            wp_reset_query();
                        endif;
                        ?>
                    </div>
                    <div class="container-fluid">
                        <h3><?php _e('Most popular albums groups', 'aletheme') ?>
                            <span><?php echo $post->post_title; ?></span>:
                        </h3>
                        <div class="container-fluid card_list album_list">
                            <?php echo get_albums($post->ID); ?>
                        </div>
                        <div class="more"><a href="#"><?php _e('All Albums', 'aletheme') ?></a></div>
                        <hr>
                    </div>

                    <div class="container-fluid lastsongs">
                        <h3><?php _e('Last Downloaded', 'aletheme') ?></h3>
                        <?php get_last_downloaded(); ?>
                    </div>

                </div>

            <?php endwhile; else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>

        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>