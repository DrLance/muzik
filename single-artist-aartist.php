<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs();
                $aartist = get_permalink($post) . 'aartist/';
                $photo = get_permalink($post) . 'photo/';
                ?>
            </ol>
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="row album_nav">
                        <!--filter start-->
                        <button class="button-a" onclick='location.href="<?php the_permalink(); ?>"'>
                            <span>
                                <?php _e('Songs', 'aletheme');
                                $childrens = get_children(array(
                                    'post_parent' => $post->ID,
                                    'post_type' => 'attachment',
                                    'numberposts' => -1,
                                    'post_mime_type' => 'audio'
                                ));
                                echo ' (' . count($childrens) . ')';
                                ?>
                            </span>
                        </button>
                        <button class="button-a" onclick='location.href="<?php echo $aartist; ?>"'>
                            <span>
                                <?php _e('Album', 'aletheme') ; echo ' (' . count(get_post_meta( $post->ID, 'albums_id' )) . ')'; ?>
                            </span>
                        </button>
                        <button class="button-a" onclick='location.href="<?php echo $photo  ; ?>"'>
                            <span>
                                <?php _e('Photo');
                                $childrens = get_children(array(
                                    'post_parent' => $post->ID,
                                    'post_type' => 'attachment',
                                    'numberposts' => -1,
                                    'post_mime_type' => 'img'
                                ));
                                echo ' (' . count($childrens) . ')';
                                ?>
                            </span>
                        </button>
                        <!--filter end-->
                    </div>
                    <div class="container-fluid">
                        <h3><?php _e('Albums Group'); ?> <span><?php the_title(); ?></span>:</h3>

                        <div class="row filter">
                            <button class="button-a btn-filter" data-target="name"><span><?php _e('Name', 'aletheme'); ?></span></button>
                            <button class="button-a btn-filter"><span><?php _e('Ratings', 'aletheme'); ?></span></button>
                            <button class="button-a btn-filter"><span><?php _e('New', 'aletheme'); ?></span></button>
                            <button class="button-a btn-filter"><span><?php _e('Year', 'aletheme'); ?></span></button>
                        </div>
                    </div>
                </div>
                <hr class="hr_list">

                <div class="container-fluid">
                    <h3><?php _e('Most popular albums groups', 'aletheme') ?> <span><?php echo $post->post_title; ?></span>:</h3>
                    <div class="container-fluid card_list album_list">
                        <?php echo get_albums($post->ID); ?>
                    </div>
                    <hr>
                </div>

            </div>
        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>