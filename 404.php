<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">

        <div class="col-md-2 left-panel side_col">
            <div class="container-fluid side_nav">
                <?php dynamic_sidebar('left-sidebar'); ?>
            </div>

        </div>
        <div class="col-md-8 content_middle">

            <div class="h2" ><?php _e('Error 404','aletheme'); ?></div>

            <div class="contact-content">

                <h1 class="errorh1"><?php _e('Error, Page not found','aletheme'); ?></h1>
                <p class="errorh1"><?php _e('Sorry, but the page you\'re looking for has not found. Try checking the URL for errors, then hit the refresh<br /> button on your browser.','aletheme'); ?></p>

            </div>
            <div class="line"></div>

            <div class="contact-footer">
                <p class="errorh1">
                    <a href="<?php echo home_url();?>" class="gohomebut"><?php _e('Return to the homepage','aletheme'); ?></a>
                </p>
            </div>

        </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>
<?php get_footer(); ?>