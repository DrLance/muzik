var mySound;
var tracks = [];
var currentId = 0;
var volume = 50;
var playlist = [];

jQuery( document ).ready(function($) {
    reloadPlayBtn();
    if($(this).find('#main').length !== 0) {
        $.ajax({
            url: myajax.url,
            type: 'post',
            data: {
                'action': 'ale_get_audios',
                'count_audio': $('#main').data('audio-cnt')
            },
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();

                $(".top_play").find('.btn_play_stop').each(function(){
                    var tUrl = $(this).data('audio');
                    tracks.push({'url': tUrl, 'btn': $(this)});
                });

                reloadPlayBtn();
            },
            success: function (html) {
                $('#main').append(html);
            }
        })
    }

    soundManager.setup({
        onready: function() {
            mySound = soundManager.createSound({
                id: 'aSound',
                onload: function() {

                },
                whileplaying: function(){
                    $('#seekObj').attr('max',this.duration).attr('value',this.position);
                },
                onfinish: function () {
                    tracks[currentId].btn.find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
                    playNext(currentId + 1);
                }
            });
        },
        defaultOptions: {
            volume: volume
        }
    });

    function reloadPlayBtn() {
        $(".top_play").find('.btn_play_stop').each(function(){
            var tUrl = $(this).data('audio');
            tracks.push({
                'url': tUrl,
                'btn': $(this)
            });
        });

        $('span[class *="save_to_pl"]').click(function (e) {
            e.preventDefault();
            var audio = $(this);
            var offset = $(this).offset();
            offset.left = offset.left + 15;
            $('#pop-playlist').css('display', 'inline-block').offset(offset);

            $('.addToPlay').on('click', function (e) {
                var playlistId = $(this).data('playlist-id');
                //e.stopPropagation();
                e.stopImmediatePropagation();
                $.ajax({
                    url: myajax.url,
                    type: 'post',
                    data: {
                        'action': 'add_track_playlist',
                        'playlist_id': playlistId,
                        'audio_id': audio.data('audio-id')
                    },
                    beforeSend: function () {

                    },
                    complete: function () {
                        $('.popup').hide();
                        $(".addToPlay").unbind();
                    },
                    success: function (html) {
                        console.log(audio);
                    }
                })

            });
        });

        $('.popup').click(function () {
            $('.popup').hide();
            $(this).removeAttr('style');
        });

        $(".btn_play_stop").click(function(e){
            btnPly = $(this).find('span');
            mySound.url = $(this).data('audio');
            if(btnPly.hasClass('glyphicon-play')) {
                if(mySound.paused) {
                    mySound.resume();
                    btnPly.removeClass('glyphicon-play').addClass('glyphicon-pause');
                    $('#play').find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');
                } else {
                    $('.player-inline').find('.glyphicon-pause').each(function(e){
                        $(this).removeClass('glyphicon-pause').addClass('glyphicon-play');
                    });

                    mySound.stop();
                    mySound.play();
                    btnPly.removeClass('glyphicon-play').addClass('glyphicon-pause');
                    $('#play').find('span').removeClass('glyphicon-play').addClass('glyphicon-pause')
                }
            } else if (btnPly.hasClass('glyphicon-pause')) {
                btnPly.removeClass('glyphicon-pause').addClass('glyphicon-play');
                $('#play').find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
                mySound.pause();
            } else {
                console.log('1111');
            }
        });

        $('.add_to_pl').click(function(e){
            console.log(e);
        });
    }

    $('#seekObj').click(function(e){
        barWidth = e.target.offsetWidth;
        newPosition = (e.offsetX / barWidth);
        if (mySound && mySound.duration) {
            mySound.setPosition(mySound.duration * newPosition);
        }
    });

    $('#play').click(function(e){
        $span = $(this).find('span');

        if($span.hasClass('glyphicon-pause')) {
            mySound.pause();
            $span.removeClass('glyphicon-pause').addClass('glyphicon-play');
            tracks[currentId].btn.find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
        } else {
            if(mySound.paused) {
                mySound.resume();
                $span.removeClass('glyphicon-play').addClass('glyphicon-pause');
                tracks[currentId].btn.find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');
            } else {
                if (tracks.length > 0) {
                    mySound.stop();
                    mySound.url = tracks[currentId].url;
                    mySound.play();
                    tracks[currentId].btn.find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');
                    $span.removeClass('glyphicon-play').addClass('glyphicon-pause');
                }
            }
        }
    });

    $('#volumeup').click(function(){
        if(mySound) {
            volume += 5;
            mySound.setVolume(volume);
        }
    });

    $('#volumedown').click(function(){
        if(mySound) {
            volume -= 5;
            mySound.setVolume(volume);
        }
    });

    $('#stop').click(function () {
        mySound.stop();
        $('#play').find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
    });

    $('#next').click(function () {
        tracks[currentId].btn.find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
       playNext(currentId+1);
    });

    $('#prev').click(function () {
        tracks[currentId].btn.find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
        playNext(currentId-1);
    });

    $("#inputSendPhoto").change(function () {
        readURL(this);
    });

    $('#userPlaylist').click(function(e){
        var $elPlaylist = $('.player-playlist');

        $elPlaylist.toggle('slow');
    });

    $("#submitSendPhoto").click(function (e) {
        console.log('Click submit');
        $.ajax({
            type:'POST',
            url:'your-post-url',
            processData: false,
            contentType: false,
            data : postData,
            success:function(data){
                console.log("File Uploaded");
            }

        });
    });

    $('#form-add-playlist').submit(function (e) {
        e.preventDefault();
        var file_data = $('#imgPlaylist')[0].files[0];
        var form_data = new FormData();
        var playlistData = {
            'playlist_name': $('#namePlaylist').val(),
            'playlist_description':$('#descriptionPlaylist').val()
        };
        form_data.append('images', file_data);
        form_data.append('playlistData', JSON.stringify(playlistData));
        form_data.append( "action", 'add_playlist');

        $.ajax({
            type: 'POST',
            url: myajax.url,
            data: form_data,
            contentType: false,
            processData: false,
            dataType : 'json',
            success: function(data) {
                window.location.href= data.url;
            }
        });
    });

    $('#deletePlaylist').click(function (e) {
        console.log($(this).data('playlist-id'));
        $.ajax({
            url: myajax.url,
            type: 'POST',
            dataType: 'json',
            data: {
                'action': 'delete_playlist',
                'playlist_id': $(this).data('playlist-id')
            },
            success: function (data) {
                window.location.href = data.url;
            }
        })

    });

    $('#formSubmitPhoto').submit(function (e) {
        e.preventDefault();
        var file_data = $('#inputSendPhoto')[0].files[0];
        var form_data = new FormData();
        var artistData = {
            'artist_id': $(this).data('artist-id')
        };
        form_data.append('images', file_data);
        form_data.append('artistData', JSON.stringify(artistData));
        form_data.append( "action", 'add_photo_artist');

        $.ajax({
            type: 'POST',
            url: myajax.url,
            data: form_data,
            contentType: false,
            processData: false,
            dataType : 'json',
            success: function(data) {
                //window.location.href= data.url;
                $('#modalSendPhoto').modal('toggle');
            }
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgSendPhoto').attr('src', e.target.result).css('display', 'block');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function playNext(id) {
        id >= tracks.length ? currentId = 0 : currentId = id;
        mySound.setPosition(0);
        mySound.url = tracks[currentId].url;
        mySound.play();
        tracks[currentId].btn.find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');
    }

});