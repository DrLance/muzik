<?php   get_header();
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once( ABSPATH . 'wp-admin/includes/media.php' );?>
    <div class="container-fluid main">
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <?php if (have_posts() && is_user_logged_in()) : while (have_posts()) :
                the_post(); ?>
                <?php
                if(isset($_POST['new_post']) == '1') {
                    $string = '<script type="text/javascript">';
                    $string .= 'window.location = "' . home_url() . '/editalbum/' . '"';
                    $string .= '</script>';
                    echo $string;
                }
                ?>
                <div class="container-fluid">
                    <h3><?php the_title(); ?></h3>
                    <div class="container-fluid new_album">
                        <p><?php _e('Current Screen', 'aletheme'); ?>:<span id="id_cover_name"> <?php _e('No Cover', 'aletheme'); ?></span></p>
                        <img id="img_file_cover" class="col-xl-5" src=""/>
                        <form enctype="multipart/form-data" method="post">
                            <div class="form-group upload_file">
                                <br>
                                <label for="submit" class="col-md-2"><?php _e('Change Screen', 'aletheme'); ?>:</label>
                                <span  class="button-a form-control">
                                    <span>
                                        <input id="input_file_cover" name="img_cover" type="file" required><?php _e('Select File', 'aletheme'); ?>
                                    </span>
                                </span>
                                <p class="help-block"><?php _e('File Must Be Image', 'aletheme'); ?></p>
                                <script type="text/javascript">

                                    jQuery("#input_file_cover").change(function () {
                                        readURL(this);
                                    });

                                    function readURL(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();

                                            reader.onload = function (e) {
                                                jQuery('#img_file_cover').attr('src', e.target.result);
                                                jQuery('#img_file_cover').css('display', 'block');
                                                jQuery('#id_cover_name').css('display','none');
                                            };

                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }

                                </script>
                            </div>
                            <div class="form-group">
                                <label for="text" class="col-md-2"><?php _e('Name', 'aletheme'); ?>:</label>
                                <input type="text" class="name_album form-control" name="post_title" required>
                            </div>
                            <div class="form-group description_album">
                                <label for="textarea" class="col-md-2"><?php _e('Description', 'aletheme'); ?>:</label>
                                <textarea class="descr-cover" name="descr_album" required></textarea>
                            </div>
                            <div class="container-fluid ">
                                <label for="option" class="col-md-2"><?php _e('Category', 'aletheme'); ?>:</label>
                                <select name="select_category" class="form-control button_2" required>
                                    <option value=""><?php _e('Select Category', 'aletheme'); ?></option>
                                    <?php ale_get_genres('category'); ?>
                                </select>
                                <div class="row selection_genre">
                                    <label for="optiont" class="col-md-2"><?php _e('Genres', 'aletheme'); ?>:</label>
                                    <select name="select_genre1" class="form-control button_3" required>
                                        <option value=""><?php _e('Select Genre', 'aletheme'); ?></option>
                                        <?php ale_get_genres('genre'); ?>
                                    </select>
                                    <select name="select_genre2" class="form-control">
                                        <option value=""><?php _e('Select Genre', 'aletheme'); ?></option>
                                        <?php ale_get_genres('genre'); ?>
                                    </select>
                                    <select name="select_genre3" class="form-control">
                                        <option value=""><?php _e('Select Genre', 'aletheme'); ?></option>
                                        <?php ale_get_genres('genre'); ?>
                                    </select>
                                </div>

                                <div class="selection_genre">
                                    <label for="option" class="col-md-2"><?php _e('Year Release', 'aletheme'); ?>:</label>
                                    <select name="select_year" class="form-control button_3">
                                        <option value=""><?php _e('Select Year', 'aletheme'); ?></option>
                                        <?php ale_get_genres('year_genre'); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="new_post" value="1"/>
                                <button type="submit" class="button-a v3 form-control"><span><?php _e('Submit', 'aletheme'); ?></span></button>
                            </div>
                        </form>
                    </div>
                </div>

                <?php if(isset($_POST['new_post']) == '1') {
                $post_title = $_POST['post_title'];
                $genre = array(
                        'genre1' => $_POST['select_genre1'],
                    'genre2' => $_POST['select_genre2'],
                    'genre3' => $_POST['select_genre3']
                );
                $description = $_POST['descr_album'];
                $category = $_POST['select_category'];

                $status = 'publish';

                if(ale_get_option('moderate_enabled')){
                    $status = 'pending';
                }

                $new_post = array(
                    'post_author' => get_current_user_id(),
                    'post_category' => array('category' => $category),
                    'post_status' => $status,
                    'post_content' => $description,
                    'post_title' => $post_title,
                    'post_type' => 'post',
                    'tax_input' => array('genre' => $genre, 'year_genre'=> $_POST['select_year'])
                );

                $post_id = wp_insert_post($new_post);

                $file = &$_FILES['img_cover'];

                $attachment_id = media_handle_upload( 'img_cover', $post_id );

                if ( is_wp_error( $attachment_id ) ) {
                    echo "Error upload file!";
                }

                set_post_thumbnail( $post_id, $attachment_id );
            }

            ?>
            <?php endwhile;
        else: ?>
            <?php ale_part('notfound'); ?>
        <?php endif; ?>
    </div>
        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">

            <?php get_sidebar('main-sidebar'); ?>

        </div>
    </div>
<?php get_footer();