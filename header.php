<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <title><?php wp_title('|', true, 'right');
        bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body>

<div class="container-fluid middle">
    <header>
        <div class="row row_1">
            <div class="col-md-2 logo"><a href="<?php echo home_url(); ?>/">
                    <img src="<?php echo ale_get_option('sitelogo');?>"  height="39" width="90%"/>
                    <span class="text"><?php _e('All Songs', 'aletheme'); ?>: <?php get_audio_count(); ?></span>
                </a></div>
            <div class="col-md-2 add_album">
                <form action="<?php echo home_url() . '/addalbum/' ?>" method="post">
                    <button type="submit" class="button-a"><?php _e('Add Album', 'aletheme'); ?></button>
                </form>
            </div>
            <div class="col-md-5">
                        <?php echo get_search_form(); ?>
            </div>
            <div class="col-md-3 aside">
                <a href="<?php echo home_url() . '/userplaylists/' ?>" class="ico-music" data-toggle="tooltip" data-original-title="<?php _e('My Playlist', 'aletheme'); ?>">
                    <!---->
                </a>
                <!--<a href="/Chat" class="ico-comment" data-toggle="tooltip" title="Чат"></a>-->
                <a href="#" class="ico-info" data-toggle="tooltip" data-original-title="<?php _e('Rules', 'aletheme'); ?>">
                    <!---->
                </a>
                <?php //if (!pmpro_hasMembershipLevel('1')) { ?>
                    <a class="button-a v2" href="membership-account/"><?php _e('Disable Advertising','aletheme') ?></a>
                <?php //} ?>
            </div>
        </div>
        <div class="row main-nav">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                        if (has_nav_menu('header_main_menu')) {
                            wp_nav_menu(array(
                                'theme_location' => 'header_main_menu',
                                'menu' => 'Header Menu',
                                'menu_class' => 'nav navbar-nav',
                                'container' => '',
                                'container_class' => '',
                                'walker' => new Aletheme_Nav_Walker(),
                            ));
                        }
                        ?>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
        <div class="row row_3">
            <ul>
                <li><a href="letter/?letter=9">0..9</a></li>
                <li><a href="letter/?letter=A">A</a></li>
                <li><a href="letter/?letter=B">B</a></li>
                <li><a href="letter/?letter=C">C</a></li>
                <li><a href="letter/?letter=D">D</a></li>
                <li><a href="letter/?letter=E">E</a></li>
                <li><a href="letter/?letter=F">F</a></li>
                <li><a href="letter/?letter=G">G</a></li>
                <li><a href="letter/?letter=H">H</a></li>
                <li><a href="letter/?letter=I">I</a></li>
                <li><a href="letter/?letter=J">J</a></li>
                <li><a href="letter/?letter=K">K</a></li>
                <li><a href="letter/?letter=L">L</a></li>
                <li><a href="letter/?letter=M">M</a></li>
                <li><a href="letter/?letter=N">N</a></li>
                <li><a href="letter/?letter=O">O</a></li>
                <li><a href="letter/?letter=P">P</a></li>
                <li><a href="letter/?letter=Q">Q</a></li>
                <li><a href="letter/?letter=R">R</a></li>
                <li><a href="letter/?letter=S">S</a></li>
                <li><a href="letter/?letter=T">T</a></li>
                <li><a href="letter/?letter=U">U</a></li>
                <li><a href="letter/?letter=V">V</a></li>
                <li><a href="letter/?letter=W">W</a></li>
                <li><a href="letter/?letter=X">X</a></li>
                <li><a href="letter/?letter=Y">Y</a></li>
                <li><a href="letter/?letter=Z">Z</a></li>
                <li><a href="letter/?letter=А">А</a></li>
                <li><a href="letter/?letter=Б">Б</a></li>
                <li><a href="letter/?letter=В">В</a></li>
                <li><a href="letter/?letter=Г">Г</a></li>
                <li><a href="letter/?letter=Д">Д</a></li>
                <li><a href="letter/?letter=Е">Е</a></li>
                <li><a href="letter/?letter=Ж">Ж</a></li>
                <li><a href="letter/?letter=З">З</a></li>
                <li><a href="letter/?letter=И">И</a></li>
                <li><a href="letter/?letter=К">К</a></li>
                <li><a href="letter/?letter=Л">Л</a></li>
                <li><a href="letter/?letter=М">М</a></li>
                <li><a href="letter/?letter=Н">Н</a></li>
                <li><a href="letter/?letter=О">О</a></li>
                <li><a href="letter/?letter=П">П</a></li>
                <li><a href="letter/?letter=Р">Р</a></li>
                <li><a href="letter/?letter=С">С</a></li>
                <li><a href="letter/?letter=Т">Т</a></li>
                <li><a href="letter/?letter=У">У</a></li>
                <li><a href="letter/?letter=Ф">Ф</a></li>
                <li><a href="letter/?letter=Х">Х</a></li>
                <li><a href="letter/?letter=Ц">Ц</a></li>
                <li><a href="letter/?letter=Ч">Ч</a></li>
                <li><a href="letter/?letter=Ш">Ш</a></li>
                <li><a href="letter/?letter=Щ">Щ</a></li>
                <li><a href="letter/?letter=Э">Э</a></li>
                <li><a href="letter/?letter=Ю">Ю</a></li>
                <li><a href="letter/?letter=Я">Я</a></li>
            </ul>
        </div>
    </header>
