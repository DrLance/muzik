<?php get_header(); ?>
    <!-- Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h3><?php the_title(); ?></h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="album_img">
                            <a href="#">
                                <?php
                                $thumb = get_the_post_thumbnail($post->ID, array(270, 250));

                                if ($thumb) {
                                    echo get_the_post_thumbnail($post->ID, array(270, 248));
                                }
                                ?>
                            </a>
                        </div>
                        <div class="container register_button">
                            <?php
                            echo '<div class="row"><a href="#" class="button-a v4"><span>' . __('Download Album', 'aletheme') . '</span></a></div>';
                            ?>
                            <?php
                            echo '<div class="row"><a href="#" class="button-a v4"><span>' . __('In Favourite', 'aletheme') . '</span></a></div>';
                            ?>
                            <?php
                            echo '<div class="row"><a href="#" class="button-a v4"><span>' . __('Listen All Album', 'aletheme') . '</span></a></div>';
                            ?>
                            <div class="center-block">

                            <?php
                            if (function_exists('the_ratings')) {
                                the_ratings();
                            } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-7">
                        <div class="container-fluid">
                            <p><span class="country"><?php _e('Genre:') ?></span>
                                <?php
                                $tmp = wp_get_post_terms($post->ID, 'genre');
                                $prx = '';
                                $lastElement = end($tmp);
                                if(count($tmp) > 1) {
                                    $prx = ' / ';
                                }
                                foreach ($tmp as $tm) {
                                    if($tm == $lastElement) {
                                        $prx = '';
                                    }
                                    echo $tm->name . $prx;
                                }

                                ?>
                            </p>
                            <p><span class="country"><?php _e('Artist:', 'aletheme') ?></span>
                                <?php
                                $post_meta = '';
                                $artists = get_posts(array(
                                        'post_type' => 'artist',
                                    'numberpost' => '-1',
                                ));
                                $user = get_user_by('id', $post->post_author);
                                $date = get_the_date( 'd.m.Y', $post->ID );

                                foreach ($artists as $artist) {
                                    $post_metas = get_post_meta($artist->ID, 'albums_id',false);
                                    foreach ($post_metas as $item) {
                                        if ($item == $post->ID) {
                                            ?>
                                            <a href="<?php echo get_page_link($artist); ?>"><?php echo $artist->post_title; ?></a>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </p>
                            <p><span class="country"><?php _e('Year:') ?></span>
                                <?php
                                $tmp = wp_get_post_terms($post->ID, 'year_genre');
                                if ($tmp) {
                                    echo $tmp[0]->name;
                                }
                                ?>
                            </p>
                            <p><span class="country"><?php _e('Upload:','aletheme'); ?></span>
                                <a href="<?php echo home_url() .  '/userprofile/?u='. $user->ID; ?>"><?php echo $user->display_name; ?></a>
                            </p>
                            <p><span class="country"><?php _e('Added:', 'aletheme'); ?></span>
                                <?php echo $date; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <h3>
                            <?php _e('All Songs in Album', 'aletheme');
                            echo ' - ' . $post->post_title; ?>
                            <span>
                            </span>
                        </h3>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="container-fluid top_play">
                    <?php
                    get_audio_for_albums($post->ID);
                    ?>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <h3>
                            <?php __('SIMILAR','aletheme'); ?>
                        </h3>
                    </div>
                    <div class="container-fluid card_list album_list">
                    <?php get_similar_albums($post->ID); ?>
                    </div>
                    <div class="container-fluid">
                        <?php comments_template(); ?>
                    </div>
                    <div class="container-fluid lastsongs">
                        <h3><?php _e('Last Downloaded', 'aletheme') ?></h3>
                        <?php get_last_downloaded(); ?>
                    </div>
                </div>

                <?php wp_reset_postdata();
                wp_reset_query(); ?>

            <?php endwhile; else: ?>
                <?php ale_part('notfound') ?>
            <?php endif; ?>

        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
<?php get_footer(); ?>