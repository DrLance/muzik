<?php
/*
  * Template name: Home
  * */
get_header();?>
    <!--Content -->
    <div class="container-fluid main">
        <!--left-panel-->
        <div class="col-md-2 left-panel side_col">
            <?php dynamic_sidebar('left-sidebar'); ?>
        </div>
        <!--left-panel-->

        <div class="col-md-8 content_middle">
            <ol class="breadcrumb">
                <?php if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(); ?>
            </ol>
            <div class="row">
                <h3><?php the_title(); ?></h3>
                <h1><?php echo ale_get_meta('descr1'); ?> </h1>
                <p class="nice-text"><?php echo ale_get_meta('descr2'); ?></p>
            </div>
            <div class="container-fluid lastsongs">
                <span class="nice-text"><?php _e('Years', 'aletheme'); ?>:</span>
                <a href="<?php the_permalink(); ?>"><?php _e('All', 'aletheme'); ?></a>
                <?php
                $args = array(
                    'type'         => 'post',
                    'child_of'     => 0,
                    'parent'       => '',
                    'orderby'      => 'name',
                    'order'        => 'ASC',
                    'hide_empty'   => 1,
                    'hierarchical' => 1,
                    'exclude'      => '',
                    'include'      => '',
                    'number'       => 0,
                    'taxonomy'     => 'year_genre',
                    'pad_counts'   => false,
                );
                $categories = get_categories( $args );
                if( $categories ){
                    foreach( $categories as $cat ){
                        global $wp;
                        $arr_params = array( 'year_genre' => $cat->name );
                        ?>
                        <a href="<?php echo esc_url( add_query_arg( $arr_params ) ); ?>"><?php echo $cat->name; ?></a>


                    <?php }
                }
                ?>
            </div>
            <!--last album-->
            <div class="container-fluid">
                <div class="container-fluid card_list album_list">
                    <?php get_albums_ost('album'); ?>
                </div>
                <hr>
            </div>
            <!--last album end-->

            <div class="container-fluid lastsongs">
                <h3><?php __('Last Downloaded', 'aletheme') ?></h3>
                <?php get_last_downloaded(); ?>
            </div>


        </div>


        <!--right-panel-->
        <div class="col-md-2 right_panel aside_col">
            <?php dynamic_sidebar('main-sidebar'); ?>
        </div>
    </div>
    <!--right-panel end-->

<?php get_footer();

